# GIG_alpha_admin_OMS

This repository is the frontend for the admin management system.

### Built With

- Node.js
- React
- React-DOM
- React-Create-App
- Redux
- npm
- CSS
- Bootstrap 5.0
- ES6
- eslint
- stylelint
- Jest
- React Testing library

## Getting Started

To get a local copy up and running follow these steps:

### Prerequisites

- npm
- Node
- create-react-app

### Setup

- Clone the repository by running the code `git clone https://gitlab.com/uimarshallgig/gig_alpha_admin_oms.git`
- cd into `gig_alpha_admin_oms.git` folder
- Run `npm install`.
- Run `npm start` from your command line to open your app in the browser.
- You can install [Redux Devtools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) in your preferred browser, I suspect this should make you run the project locally, if you encounter any issue.

Run the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Git Work Flow

Feature branches
Leverage [feature branches](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) whenever you want to add a feature or fix a bug.

1. Pull the most recent development.
2. Create a `feature/bug` branch with the following pattern `feature_[your name]_[name of feature]`
3. Example => `feature_Marshall_add-new-modal` or `bug_Marshall_fix-broken-navigation`

## Commit and Push

When working on a feature make sure to commit and push your changes to the remote branch as often as possible.

This assures that your commits are small and manageable.
Additionally, in the event of system failure or any random that affects your local code, you will have a fairly recent backup of your code in the remote branch.

## Before Pull Requests

Before you create a pull request, run the following commands locally:

`npm run lint:fix:` to check your code for linting errors. There should be zero.

`npm run test:` if there are tests, they should all be passing.

`npm run build:` to make sure that your code will successfully build in production.

`Pull Requests (PR)`

After you have finished working on your feature or bug fix, you should create a pull request from your feature branch to the development branch.

This PR or MR allows you to see the difference between the current development code and the feature code that you want to merge into the master.
This process is very important because it allows your teammates to review your code in order make sure that it is consistent with coding conventions and make potential suggestions to improve the code quality.

## Pull Request Maximum Changes

Each PR should contain small and specific code changes. It is difficult for your teammates to review your code when they are many changes all pushed into one Pull Request. Therefore, you must absolutely abide by the following rules:

All Pull Requests can contain at most `250 changes`.
If your PR contains more than `250 changes`, you must break down this PR into smaller different PRs.
Review All Pull Requests
It is required that each member of your team review and comment on every PR that is created. This makes sure that:

You are keeping your teammates accountable for the quality of the code that they are writing.
The code merged into your master meets the highest of standards.
You are learning from the code that your teammates are writing.

### To Fix linting errors

- Run `npm run lint:fix`

### Testing

- cd into `gig_alpha_admin_oms.git` folder

- Run `npm test`

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

### Deployment

The app is currently deployed on netlify for testing.

## Authors

#### 👤 **Marshall AKPAN**

- Github: [@uimarshall](https://github.com/uimarshall)
- Twitter: [@uimarshall](https://twitter.com/uimarshall)
- Linkedin: [uimarshall](https://www.linkedin.com/in/marshall-akpan-19745526/)

## 🤝 Contributing

Our favourite contributions are those that help us improve the project, whether with a contribution, an issue, or a feature request!

Feel free to check the [issues page](https://gitlab.com/uimarshallgig/gig_alpha_admin_oms/-/issuess) to either create an issue or help us out by fixing an existing one.

## Acknowledgements
