/* eslint-disable  no-unused-vars */
import { useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import jwtDecode from 'jwt-decode';
import setAuthToken from './components/shared/setAuthToken';
import store from './store';

// import Menubar from './components/layout/Menubar';
import Navbar from './components/layout/Navbar';

import Routes from './components/router/routes';
import './App.css';
import { loadCurrentLoggedInUser } from './actions/userActions';
import Footer from './components/layout/Footer';
import NavMenu from './components/layout/NavMenu';
import Navbar2 from './components/layout/Navbar2';
import Header from './components/layout/header/Header';

// Check for token
if (localStorage.jwtToken) {
  // Set auth token header
  setAuthToken(localStorage.jwtToken);
  // Decode token to get user info/expiration
  const decoded = jwtDecode(localStorage.jwtToken);
  // Load current loggedInUser
  store.dispatch(loadCurrentLoggedInUser(decoded));
}
function App() {
  // useEffect(() => {
  //   // Check for token
  //   if (localStorage.jwtToken) {
  //     // Set auth token header
  //     setAuthToken(localStorage.jwtToken);
  //     // Decode token to get user info/expiration
  //     const decoded = jwtDecode(localStorage.jwtToken);
  //     // Load current loggedInUser
  //     store.dispatch(loadCurrentLoggedInUser(decoded));
  //   }
  // }, []);
  return (
    <Router>
      <div className="App">
        {/* <Menubar /> */}
        {/* <Navbar /> */}
        {/* <NavMenu /> */}
        {/* <Navbar2 /> */}
        <Header />
        <div id="content-wrap">
          <Routes />
        </div>

        <Footer />
      </div>
    </Router>
  );
}

export default App;
