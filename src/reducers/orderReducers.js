/* eslint-disable import/prefer-default-export */
/* eslint-disable  no-unused-vars */

import {
  ORDER_LOGIN_REQUEST,
  ORDER_LOGIN_SUCCESS,
  ORDER_LOGIN_FAILURE,
  ALL_HOME_DELIVERY_ADDRESS_REQUEST,
  ALL_HOME_DELIVERY_ADDRESS_SUCCESS,
  ALL_HOME_DELIVERY_ADDRESS_FAILURE,
  CLEAR_ERRORS,
  SHIPMENT_PRICE_REQUEST,
  SHIPMENT_PRICE_SUCCESS,
  SHIPMENT_PRICE_FAILURE,
  ADD_PRODUCT_TO_STATION,
  GET_STATION_INFO_REQUEST,
  INITIALIZE_TRANSACTION_REQUEST,
  INITIALIZE_TRANSACTION_SUCCESS,
  INITIALIZE_TRANSACTION_FAILURE,
  PAYMENT_METHOD_SUCCESS,
  PAYMENT_METHOD_REQUEST,
  PAYMENT_METHOD_FAILURE,
  VERIFY_TRANSACTION_REQUEST,
  VERIFY_TRANSACTION_SUCCESS,
  VERIFY_TRANSACTION_FAILURE,
  ALL_SERVICE_CENTERS_REQUEST,
  ALL_PRODUCTS_SUCCESS,
  ALL_SERVICE_CENTERS_SUCCESS,
  ALL_SERVICE_CENTERS_FAILURE,
} from '../actions/actionTypes';

const initialState = {
  addresses: [],

  loading: false,
};

export const orderShipmentAuthReducer = (
  state = {
    status: {},
    loading: false,
    isAuthenticated: false,
  },
  { type, payload }
) => {
  switch (type) {
    case ORDER_LOGIN_REQUEST:
      return { ...state, loading: true, isAuthenticated: false };

    case ORDER_LOGIN_SUCCESS:
      return { ...state, loading: false, isAuthenticated: true, status: payload };

    case ORDER_LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
        error: payload,
      };

    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

// Shipment Info Items

export const stationReducer = (state = { stationItems: [], stationInfo: {} }, action) => {
  switch (action.type) {
    case ADD_PRODUCT_TO_STATION:
      return { ...state, stationItems: action.payload };

    case GET_STATION_INFO_REQUEST:
      return {
        ...state,
        stationInfo: action.payload,
      };

    default:
      return state;
  }
};

// Post Shipment price data

export const shipmentPriceReducer = (
  state = { shipmentPriceDetails: {}, loading: false },
  { type, payload }
) => {
  switch (type) {
    case SHIPMENT_PRICE_REQUEST:
      return { ...state, loading: true };

    case SHIPMENT_PRICE_SUCCESS:
      return { ...state, loading: false, shipmentPriceDetails: payload };

    case SHIPMENT_PRICE_FAILURE:
      return {
        ...state,
        loading: false,

        shipmentPriceDetails: null,
        error: payload,
      };

    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

// Post Transaction data

export const initializeTransactionReducer = (
  state = { transactionInfo: {}, loading: false },
  { type, payload }
) => {
  switch (type) {
    case INITIALIZE_TRANSACTION_REQUEST:
      return { ...state, loading: true };

    case INITIALIZE_TRANSACTION_SUCCESS:
      return { ...state, loading: false, transactionInfo: payload };

    case INITIALIZE_TRANSACTION_FAILURE:
      return {
        ...state,
        loading: false,

        transactionInfo: null,
        error: payload,
      };

    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};
// Post Transaction Verification data

export const verifyTransactionReducer = (
  state = { transactVerificationInfo: {}, loading: false },
  { type, payload }
) => {
  switch (type) {
    case VERIFY_TRANSACTION_REQUEST:
      return { ...state, loading: true };

    case VERIFY_TRANSACTION_SUCCESS:
      return { ...state, loading: false, transactVerificationInfo: payload };

    case VERIFY_TRANSACTION_FAILURE:
      return {
        ...state,
        loading: false,

        transactVerificationInfo: null,
        error: payload,
      };

    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

// Get payment method Data

export const paymentMethodReducer = (
  state = { paymentMethods: {}, loading: false },

  action
) => {
  switch (action.type) {
    case PAYMENT_METHOD_REQUEST:
      return { loading: true, paymentMethods: {} };

    case PAYMENT_METHOD_SUCCESS:
      return {
        ...state,
        loading: false,
        paymentMethods: action.payload,
      };

    case PAYMENT_METHOD_FAILURE:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export const homeDeliveryReducer = (state = initialState, action) => {
  switch (action.type) {
    case ALL_HOME_DELIVERY_ADDRESS_REQUEST:
      return { loading: true, addresses: [] };

    case ALL_HOME_DELIVERY_ADDRESS_SUCCESS:
      return {
        ...state,
        loading: false,
        addresses: action.payload.results.Object,
      };

    case ALL_HOME_DELIVERY_ADDRESS_FAILURE:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

// Service Centers
export const serviceCentersReducer = (
  state = { loading: false, serviceCenters: [] },
  action
) => {
  switch (action.type) {
    case ALL_SERVICE_CENTERS_REQUEST:
      return { loading: true, serviceCenters: [] };

    case ALL_SERVICE_CENTERS_SUCCESS:
      return {
        ...state,
        loading: false,
        serviceCenters: action.payload.results.Object,
      };

    case ALL_SERVICE_CENTERS_FAILURE:
      return {
        loading: false,
        error: action.payload,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};
