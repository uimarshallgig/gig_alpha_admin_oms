import { combineReducers } from 'redux';
import { cartReducer } from './cartReducers';
import {
  orderShipmentAuthReducer,
  shipmentPriceReducer,
  stationReducer,
  initializeTransactionReducer,
  homeDeliveryReducer,
  paymentMethodReducer,
  verifyTransactionReducer,
  serviceCentersReducer,
} from './orderReducers';
import { productDetailReducer, productsReducer } from './productReducers';
import { authReducer, regularUserReducer, userRolesReducer } from './userReducers';

const rootReducer = combineReducers({
  products: productsReducer,
  auth: authReducer,
  role: userRolesReducer,
  productDetails: productDetailReducer,
  regularUser: regularUserReducer,
  cart: cartReducer,
  shipping: orderShipmentAuthReducer,
  paymentMethod: paymentMethodReducer,
  shippingPrice: shipmentPriceReducer,
  homeDelivery: homeDeliveryReducer,
  station: stationReducer,
  initializeTransaction: initializeTransactionReducer,
  verifyTransaction: verifyTransactionReducer,
  serviceCenters: serviceCentersReducer,
});

export default rootReducer;
