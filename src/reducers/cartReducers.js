/* eslint-disable import/prefer-default-export */
import { SAVE_SHIPPING_INFO, CLEAR_ERRORS } from '../actions/actionTypes';

// const initialState = {
//   status: '',
//   loading: false,
//   isAuthenticated: false,
// };

export const cartReducer = (state = { shippingInfo: {} }, action) => {
  switch (action.type) {
    case SAVE_SHIPPING_INFO:
      return {
        ...state,
        shippingInfo: action.payload,
      };

    case CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};
