import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import thunk from 'redux-thunk';
import rootReducer from './reducers';

// The initialState contains all the data we want to put into the state just b4 the application loads.
// In the initialState, we have to put in all the products from the localStorage
const initialState = {
  station: {
    stationItems: localStorage.getItem('stationItems')
      ? JSON.parse(localStorage.getItem('stationItems'))
      : [],
    stationInfo: localStorage.getItem('stationInfo')
      ? JSON.parse(localStorage.getItem('stationInfo'))
      : {},
  },
};

const middleware = [thunk];
const enhancer = composeWithDevTools(applyMiddleware(...middleware));
const store = createStore(rootReducer, initialState, enhancer);

export default store;
