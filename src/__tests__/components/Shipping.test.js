import * as React from 'react';
import '@testing-library/jest-dom';
import { render, cleanup } from '@testing-library/react';
import renderer from 'react-test-renderer';
import Shipping from '../../components/cart/Shipping';

jest.mock('../../components/cart/Shipping', () => () => (
  <div id="shipping">Shipping</div>
));
describe('Shipping Component', () => {
  test('renders Shipping in the DOM', () => {
    const { container } = render(<Shipping />);
    const mockComponent = container.querySelector('div#shipping');

    expect(mockComponent).toBeInTheDocument();
    expect(mockComponent).not.toHaveTextContent(/^arbitrary$/); // to match the whole content
    expect(mockComponent).not.toHaveTextContent(/fire$/i); // to use case-insensitive match
    expect(mockComponent).toHaveTextContent('Shipping');
  });
});

describe('Shipping Component', () => {
  afterEach(cleanup);

  it('should take a snapshot for Shipping Component', () => {
    const asFragment = renderer.create(<Shipping />).toJSON();
    expect(asFragment).toMatchSnapshot();
  });
});
