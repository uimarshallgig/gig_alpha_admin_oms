import { render, screen } from '@testing-library/react';
import MetaData from '../../components/shared/MetaData';

describe('MetaData Component', () => {
  test('renders MetaData component without crashing', () => {
    const nonExist = 'No such text';
    render(<MetaData title="Product details" />);

    expect(screen.queryByText(nonExist)).toBeNull();
  });
});
describe('Proper Rendering', () => {
  test('renders MetaData component correctly', () => {
    render(<MetaData title="Product details" />);

    expect(screen.queryByText(/Run faster now/)).toBeNull();
  });
});

// test('should render same text passed into title props', async () => {
//   render(<MetaData title="login" />);
//   const metaDataElement = screen.getByText(/login/i);
//   expect(metaDataElement).toBeInTheDocument();
// });
