import * as React from 'react';
import '@testing-library/jest-dom';
import { render, cleanup } from '@testing-library/react';
import renderer from 'react-test-renderer';
import ConfirmOrder from '../../components/cart/ConfirmOrder';

jest.mock('../../components/cart/ConfirmOrder', () => () => (
  <div id="ConfirmOrder">ConfirmOrder</div>
));
describe('ConfirmOrder Component', () => {
  test('renders ConfirmOrder in the DOM', () => {
    const { container } = render(<ConfirmOrder />);
    const mockComponent = container.querySelector('div#ConfirmOrder');

    expect(mockComponent).toBeInTheDocument();
    expect(mockComponent).not.toHaveTextContent(/^arbitrary$/); // to match the whole content
    expect(mockComponent).not.toHaveTextContent(/fire$/i); // to use case-insensitive match
    expect(mockComponent).toHaveTextContent('ConfirmOrder');
  });
});

describe('ConfirmOrder Component', () => {
  afterEach(cleanup);

  it('should take a snapshot for ConfirmOrder Component', () => {
    const asFragment = renderer.create(<ConfirmOrder />).toJSON();
    expect(asFragment).toMatchSnapshot();
  });
});
