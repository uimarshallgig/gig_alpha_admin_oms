/* eslint-disable no-unused-vars */
/* eslint-disable import/prefer-default-export */
/* eslint-disable no-underscore-dangle */

import axios from 'axios';
import {
  ORDER_LOGIN_REQUEST,
  ORDER_LOGIN_SUCCESS,
  ORDER_LOGIN_FAILURE,
  ALL_HOME_DELIVERY_ADDRESS_REQUEST,
  ALL_HOME_DELIVERY_ADDRESS_SUCCESS,
  ALL_HOME_DELIVERY_ADDRESS_FAILURE,
  SHIPMENT_PRICE_REQUEST,
  SHIPMENT_PRICE_SUCCESS,
  SHIPMENT_PRICE_FAILURE,
  CLEAR_ERRORS,
  ADD_PRODUCT_TO_STATION,
  GET_STATION_INFO_REQUEST,
  INITIALIZE_TRANSACTION_REQUEST,
  INITIALIZE_TRANSACTION_SUCCESS,
  INITIALIZE_TRANSACTION_FAILURE,
  PAYMENT_METHOD_REQUEST,
  PAYMENT_METHOD_FAILURE,
  PAYMENT_METHOD_SUCCESS,
  VERIFY_TRANSACTION_REQUEST,
  VERIFY_TRANSACTION_SUCCESS,
  VERIFY_TRANSACTION_FAILURE,
  ALL_SERVICE_CENTERS_REQUEST,
  ALL_SERVICE_CENTERS_SUCCESS,
  ALL_SERVICE_CENTERS_FAILURE,
} from './actionTypes';

// Order Login
export const orderShipmentLogin = () => async (dispatch) => {
  try {
    dispatch({ type: ORDER_LOGIN_REQUEST });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post(
      'https://alpha-order-sandbox-api.com/api/v1/order/login',

      config
    );

    dispatch({
      type: ORDER_LOGIN_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ORDER_LOGIN_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Get GIGGO local stations
export const addItemsToGigLocStations = (id) => async (dispatch, getState) => {
  const { data } = await axios.get(
    `https://alpha-product-sandbox.com/api/v1/product/${id}`
  );

  dispatch({
    type: ADD_PRODUCT_TO_STATION,
    payload: {
      productId: data.results._id,
      productName: data.results.productName,
      location: data.results.location,
      address: data.results.address,
      neighborhood: data.results.neighborhood,
      name: data.results.merchant_first_name,
      price: data.results.price,
      locality: data.results.locality,
      merchantStationId: data.results.merchant_station_id,
      merchantPhoneNumber: data.results.merchant_phone_number,
      customerCode: data.results.customer_code,
      weight: data.results.weight,
      lat: data.results.lat,
      long: data.results.long,
      userId: data.results.createdBy,
      quantity: data.results.quantity,
    },
  });
  // Convert the JSON objects to string for localStorage
  // Save stationItems to localStorage
  // Load items in the stationItems from localStorage incase of refresh
  // Pull whatever is in the state(getState()),take out the stationItems and save it to localStorage
  localStorage.setItem('stationItems', JSON.stringify(getState().station.stationItems));
};

export const getStationInfo = () => async (dispatch) => {
  const { data } = await axios.get(
    `https://alpha-order-sandbox-api.com/api/v1/order/stations`
  );
  dispatch({
    type: GET_STATION_INFO_REQUEST,
    payload: data.results,
  });

  localStorage.setItem('stationInfo', JSON.stringify(data));
};

// Shipment Price
export const shipmentPrice = (shipmentData) => async (dispatch) => {
  try {
    dispatch({ type: SHIPMENT_PRICE_REQUEST });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post(
      'https://alpha-order-sandbox-api.com/api/v1/order/shipping_price',
      shipmentData,
      config
    );

    dispatch({
      type: SHIPMENT_PRICE_SUCCESS,
      payload: data.results,
    });
  } catch (error) {
    dispatch({
      type: SHIPMENT_PRICE_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Initialize Payment
export const initializeTransaction = (transactionData) => async (dispatch) => {
  try {
    dispatch({ type: INITIALIZE_TRANSACTION_REQUEST });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post(
      'https://alpha-order-sandbox-api.com/api/v1/transaction/initiate',
      transactionData,
      config
    );
    console.log('data from orderAction', data);

    dispatch({
      type: INITIALIZE_TRANSACTION_SUCCESS,
      payload: data.results,
    });
  } catch (error) {
    dispatch({
      type: INITIALIZE_TRANSACTION_FAILURE,
      // payload: error.response.data.message,
      payload: console.log(error),
    });
  }
};

// Verify Payment
export const verifyTransaction = (transactVerifyData) => async (dispatch) => {
  try {
    dispatch({ type: VERIFY_TRANSACTION_REQUEST });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post(
      'https://alpha-order-sandbox-api.com/api/v1/transaction/verify',
      transactVerifyData,
      config
    );
    console.log('data from orderAction', data);

    dispatch({
      type: VERIFY_TRANSACTION_SUCCESS,
      payload: data.results,
    });
  } catch (error) {
    dispatch({
      type: VERIFY_TRANSACTION_FAILURE,
      payload: error.response,
      // payload1: console.log(
      //   'Error from transactionVerifyAPI: ',
      //   error.response.statusText
      // ),
    });
  }
};

// Payment Method

export const getPaymentMethod = () => async (dispatch) => {
  try {
    dispatch({ type: PAYMENT_METHOD_REQUEST });

    const link = `https://alpha-order-sandbox-api.com/api/v1/gateway/list`;

    const { data } = await axios.get(link);

    dispatch({
      type: PAYMENT_METHOD_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: PAYMENT_METHOD_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const getHomeDeliveryAddress = () => async (dispatch) => {
  try {
    dispatch({ type: ALL_HOME_DELIVERY_ADDRESS_REQUEST });

    const link = `https://alpha-order-sandbox-api.com/api/v1/order/homedelivery_address`;

    const { data } = await axios.get(link);

    dispatch({
      type: ALL_HOME_DELIVERY_ADDRESS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_HOME_DELIVERY_ADDRESS_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Service Centers
export const getServiceCenters = (id) => async (dispatch) => {
  try {
    dispatch({ type: ALL_SERVICE_CENTERS_REQUEST });

    const link = `https://alpha-order-sandbox-api.com/api/v1/order/service_centers?stationId=${id}`;

    const { data } = await axios.get(link);

    dispatch({
      type: ALL_SERVICE_CENTERS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_SERVICE_CENTERS_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Clear errors
export const clearErrors = () => async (dispatch) => {
  dispatch({
    type: CLEAR_ERRORS,
  });
};
