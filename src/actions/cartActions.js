/* eslint-disable import/prefer-default-export */

// import axios from 'axios';
import { SAVE_SHIPPING_INFO } from './actionTypes';

export const saveShippingInfo = (data) => async (dispatch) => {
  dispatch({
    type: SAVE_SHIPPING_INFO,
    payload: data,
  });

  localStorage.setItem('shippingInfo', JSON.stringify(data));
};
