/* eslint-disable no-unused-vars */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import jwtDecode from 'jwt-decode';

import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  CLEAR_ERRORS,
  SIGN_UP_USER_REQUEST,
  SIGN_UP_USER_SUCCESS,
  SIGN_UP_USER_FAILURE,
  GET_USER_ROLES_REQUEST,
  GET_USER_ROLES_SUCCESS,
  GET_USER_ROLES_FAILURE,
  LOAD_USER_REQUEST,
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILURE,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  UPDATE_PROFILE_REQUEST,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAILURE,
  UPDATE_PROFILE_RESET,
  UPDATE_PASSWORD_REQUEST,
  UPDATE_PASSWORD_SUCCESS,
  UPDATE_PASSWORD_FAILURE,
  UPDATE_PASSWORD_RESET,
  ALL_REGULAR_USERS_REQUEST,
  ALL_REGULAR_USERS_SUCCESS,
  ALL_REGULAR_USERS_FAILURE,
} from './actionTypes';

import setAuthToken from '../components/shared/setAuthToken';

// Load/Set Currently Logged In User
export const loadCurrentLoggedInUser = (decoded) => async (dispatch) => {
  try {
    dispatch({ type: LOAD_USER_REQUEST });

    dispatch({
      type: LOAD_USER_SUCCESS,
      payload: decoded,
    });
  } catch (error) {
    dispatch({
      type: LOAD_USER_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Login
export const loginUser = (email, password) => async (dispatch) => {
  try {
    dispatch({ type: LOGIN_REQUEST });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post(
      'https://alpha-user-sandbox-api.com/api/v1/auth/login',
      { email, password }, // send along the email and password received from forms
      config
    );

    // Save to localStorage
    const { token } = data.results;

    // Set token to localStorage.
    localStorage.setItem('jwtToken', `Bearer ${token}`);
    setAuthToken(token);
    // Decode token to get userData
    const decoded = jwtDecode(token);
    // console.log(decoded);
    // Set current user
    dispatch(loadCurrentLoggedInUser(decoded));

    dispatch({
      type: LOGIN_SUCCESS,
      payload: data.results,
    });
  } catch (error) {
    dispatch({
      type: LOGIN_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Get User Roles
export const getUserRoles = () => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_ROLES_REQUEST });

    const { data } = await axios.get(
      'https://alpha-user-sandbox-api.com/api/v1/role/all'
    );

    // console.log(data);

    dispatch({
      type: GET_USER_ROLES_SUCCESS,
      payload: data.results,
    });
  } catch (error) {
    dispatch({
      type: GET_USER_ROLES_SUCCESS,
      payload: error.response.data.message,
    });
  }
};

// Register User/Create User
export const registerUser = (userData) => async (dispatch) => {
  try {
    dispatch({ type: SIGN_UP_USER_REQUEST });

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.post(
      'https://alpha-user-sandbox-api.com/api/v1/auth/admin',
      userData,
      config
    );

    dispatch({
      type: SIGN_UP_USER_SUCCESS,
      payload: data.results,
    });
  } catch (error) {
    dispatch({
      type: SIGN_UP_USER_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Logout user
export const logoutUser = () => async (dispatch) => {
  try {
    await axios.get('https://alpha-user-sandbox-api.com/api/v1/auth/logout');

    // Set token to localStorage.
    await localStorage.removeItem('jwtToken');
    // Remove auth header
    setAuthToken(false);

    // dispatch(loadCurrentLoggedInUser(decoded));

    dispatch({
      type: LOGOUT_SUCCESS,
    });
  } catch (error) {
    dispatch({
      type: LOGOUT_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// REGULAR USERS-MERCHANT

export const getAllRegularUsers = () => async (dispatch) => {
  try {
    dispatch({ type: ALL_REGULAR_USERS_REQUEST });

    const link = `https://alpha-user-sandbox-api.com/api/v1/user/all`;

    const { data } = await axios.get(link);
    console.log(data);

    dispatch({
      type: ALL_REGULAR_USERS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_REGULAR_USERS_FAILURE,
      payload: error.response.data.message,
    });
  }
};

// Clear errors
export const clearErrors = () => async (dispatch) => {
  dispatch({
    type: CLEAR_ERRORS,
  });
};
