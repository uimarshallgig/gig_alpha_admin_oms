/* eslint-disable no-unused-vars */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import { Link } from 'react-router-dom';

import './Navbar.scss';
import { logoutUser } from '../../actions/userActions';

const Navbar = () => {
  const alert = useAlert();
  const dispatch = useDispatch();

  const { user, loading, isAuthenticated } = useSelector((state) => state.auth);
  console.log(user);

  const logoutHandler = () => {
    dispatch(logoutUser());
    alert.success('Logged out successfully.');
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light  fixed-top">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <img
              src="assets/images/gig_logo.png"
              width="34"
              height="34"
              className="d-inline-block align-top"
              alt="gig logo"
            />
          </Link>

          {/* toggle button for mobile nav */}
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#toggleMobileMenu"
            aria-controls="toggleMobileMenu"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <form className="form-inline navbar-center my-2">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
          </form>

          {isAuthenticated && user ? (
            <div
              className="collapse navbar-collapse justify-content-end align-center"
              id="toggleMobileMenu"
              Link
            >
              <ul className="navbar-nav my-2 my-lg-0">
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="!#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    {isAuthenticated && user.email}
                  </a>
                  <div
                    className="dropdown-menu dropdown-menu-lg-right"
                    aria-labelledby="navbarDropdown"
                  >
                    {/* <h6 className="dropdown-header">Dropdown header</h6> */}
                    {user.role && user.role.name === 'super admin' && (
                      <>
                        <Link className="dropdown-item" to="/dashboard">
                          Dashboard
                        </Link>
                      </>
                    )}
                    {user.role && user.role.name === 'super admin' && (
                      <>
                        <Link
                          className="btn btn-light action-button"
                          role="button"
                          to="/register"
                        >
                          Create Users
                        </Link>
                      </>
                    )}
                    <Link className="dropdown-item" to="/orders/me">
                      Orders
                    </Link>
                    <Link className="dropdown-item" to="/me">
                      Profile
                    </Link>
                    <Link
                      className="dropdown-item text-danger"
                      to="/"
                      onClick={logoutHandler}
                    >
                      Logout
                    </Link>
                  </div>
                </li>
              </ul>
            </div>
          ) : (
            !loading && (
              <>
                <span className="navbar-text ms-auto me-2">
                  {' '}
                  <Link
                    to="/products"
                    className="text-muted fw-bold text-decoration-none"
                  >
                    Products
                  </Link>
                </span>
                <span className="navbar-text mr-2">
                  {' '}
                  <Link
                    to="/login"
                    className="login btn btn-outline-success rounded-pill fw-bold"
                  >
                    Log In
                  </Link>
                </span>
              </>
            )
          )}
        </div>
      </nav>
    </>
  );
};

export default Navbar;
