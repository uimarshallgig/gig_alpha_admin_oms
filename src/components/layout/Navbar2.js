/* eslint-disable react/self-closing-comp */
import React from 'react';
import './Navbar2.css';
// import SearchWithDropDown from './SearchWithDropDown';

const Navbar2 = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark-purple mb-2">
        <div className="container-fluid">
          <a className="navbar-brand" href="!#">
            <img
              src="/assets/images/gig_logo_3.png"
              width="128"
              height="86"
              className="d-inline-block align-top"
              alt="gig logo"
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            {/* <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="!#">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="!#">
                  Link
                </a>
              </li>
            </ul> */}
            {/* <SearchWithDropDown /> */}
            {/* <form className="search-wrap me-auto">
              <div className="input-group w-100">
                <input
                  type="text"
                  className="form-control search-form"
                  placeholder="Search"
                />
                <div className="input-group-append">
                  {' '}
                  <button className="btn btn-light search-button p-2" type="submit">
                    {' '}
                    <i className="fa fa-search" />{' '}
                  </button>
                </div>
              </div>
            </form> */}
            {/* <ul className="navbar-nav ms-auto">
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="!#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="bi bi-person-circle text-white"></i> Login
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li>
                    <a className="dropdown-item" href="!#">
                      Action
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="!#">
                      Another action
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="!#">
                      Something else here
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="!#">
                  <i className="bi bi-cart-fill text-white"></i>
                </a>
              </li>
              <li className="nav-item">
                <button type="button" className="btn btn-success btn-sm nav-link">
                  Seller
                </button>
              </li>
            </ul> */}
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar2;
