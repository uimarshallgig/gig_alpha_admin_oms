/* eslint-disable react/style-prop-object */

import React from 'react';
import './SearchWithDropDown.css';

const SearchWithDropDown = () => {
  return (
    <>
      <div
        id="page-header-search"
        className="search-container input-group"
        style={{ width: 'auto' }}
      >
        <div className="input-group-append home_page_search_height">
          <div className="search_loupe_btn search_loupe_btn_border nav_bar_search_buttons da_nav_search_common_background">
            <svg className="fas fa-search">
              <use href="#fas.fa-search" />
            </svg>
          </div>
        </div>
        <input
          id="search-input"
          type="text"
          className="form-control search_input_nav_bar home_page_search_height da_nav_search_common_background search_input_nav_bar_border"
          maxLength="200"
          placeholder="Search..."
          title="Search for resources"
        />
        <div className="input-group-append home_page_search_height">
          <div
            className="nav_bar_search_input_dropdown_arrow nav_bar_search_buttons da_nav_search_common_background nav_bar_search_input_dropdown_arrow_border"
            style="width: 38px; padding-left: 14px; display: flex;"
          >
            <svg className="fas fa-angle-down">
              <use href="#fas.fa-angle-down" />
            </svg>
          </div>
        </div>
      </div>
    </>
  );
};

export default SearchWithDropDown;
