import React, { Suspense, lazy } from 'react';
import TopNav from '../../dashboard/TopNav';

const Header = () => {
  const pathUrl = window.location.pathname;
  const DesktopHeader = lazy(() => import('../Navbar2'));

  return (
    <>
      {pathUrl === '/' ? (
        <TopNav />
      ) : (
        <Suspense fallback={<TopNav />}>
          <DesktopHeader />
        </Suspense>
      )}
    </>
  );
};

export default Header;
