import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const SearchBox = () => {
  return (
    <>
      <form className="form-inline">
        <input
          className="form-control mr-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
        />
        <button className="btn btn-info" type="submit">
          <FontAwesomeIcon icon={faSearch} />
        </button>
      </form>
    </>
  );
};

export default SearchBox;
