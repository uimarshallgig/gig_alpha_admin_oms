/* eslint-disable react/self-closing-comp */
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <>
      <section className="pt-3 text-white main-footer" id="main-footer">
        <div className="container">
          <div className="row justify-content-around">
            <div className="col-6 col-lg-2 mb-3">
              <h5 className="lh-lg fw-bold text-1000">Company</h5>
              <ul className="list-unstyled mb-md-4 mb-lg-0">
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    About Us
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Find a Store
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Rules and Terms
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Sitemap
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-6 col-lg-2 mb-3">
              <h5 className="lh-lg fw-bold text-1000">Help</h5>
              <ul className="list-unstyled mb-md-4 mb-lg-0">
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Contact Us
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Shipping Info
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Money Refunds
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    How to Order
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Order Status
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Open Dispute
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-6 col-lg-2 mb-3">
              <h5 className="lh-lg fw-bold text-1000">Account</h5>
              <ul className="list-unstyled mb-md-4 mb-lg-0">
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="/login">
                    Login
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link
                    className="text-800 text-decoration-none text-white"
                    to="/register"
                  >
                    Register
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    Account Setting
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    My Orders
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-6 col-lg-2 mb-3 socials">
              <h5 className="lh-lg fw-bold text-1000">Social</h5>
              <ul className="list-unstyled mb-md-4 mb-lg-0">
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    <i className="bi bi-facebook me-1"></i> Facebook
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    <i className="bi bi-twitter me-1"></i> Twitter
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    <i className="bi bi-instagram me-1"></i> Instagram
                  </Link>
                </li>
                <li className="lh-lg">
                  <Link className="text-800 text-decoration-none text-white" to="#!">
                    <i className="bi bi-youtube me-1"></i> Youtube
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="border-bottom border-1" />
          <footer className="py-1">
            <p className="text-center mt-1">&copy;GIGAlpha. All Rights Reserved</p>
          </footer>
        </div>
      </section>
    </>
  );
};

export default Footer;
