/* eslint-disable react/self-closing-comp */
/* eslint-disable jsx-a11y/control-has-associated-label */

import React from 'react';
import './NavMenu.css';

const NavMenu = () => {
  return (
    <>
      <header className="section-header">
        <section className="header-main border-bottom">
          <div className="container-fluid">
            <div className="row align-items-center">
              <div className="col-lg-3 col-sm-4 col-md-4 col-5">
                {' '}
                <a href="!#" className="brand-wrap" data-abc="true">
                  <span className="logo">BBBOOTSTRAP</span>{' '}
                </a>{' '}
              </div>
              <div className="col-lg-4 col-xl-5 col-sm-8 col-md-4 d-none d-md-block">
                <form action="#" className="search-wrap">
                  <div className="input-group w-100">
                    {/* <input
                      type="text"
                      className="form-control search-form"
                      placeholder="Search"
                    />
                    <div className="input-group-append">
                      {' '}
                      <button className="btn btn-primary search-button" type="submit">
                        {' '}
                        <i className="fa fa-search"></i>{' '}
                      </button>
                    </div> */}

                    <div className="input-group">
                      <div className="input-group-append">
                        {' '}
                        <button className="btn btn-primary search-button" type="submit">
                          {' '}
                          <i className="fa fa-search"></i>{' '}
                        </button>
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        aria-label="Text input with 2 dropdown buttons"
                      />
                      <button
                        className="btn btn-info w-25 dropdown-toggle"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        All categories
                      </button>
                      <ul className="dropdown-menu dropdown-menu-end">
                        <li>
                          <a className="dropdown-item" href="!#">
                            Action
                          </a>
                        </li>
                        <li>
                          <a className="dropdown-item" href="!#">
                            Another action
                          </a>
                        </li>
                        <li>
                          <a className="dropdown-item" href="!#">
                            Something else here
                          </a>
                        </li>
                        <li>
                          <hr className="dropdown-divider" />
                        </li>
                        <li>
                          <a className="dropdown-item" href="!#">
                            Separated link
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </form>
              </div>
              <div className="col-lg-5 col-xl-4 col-sm-8 col-md-4 col-7">
                <div className="d-flex justify-content-end">
                  {' '}
                  <a
                    target="_blank"
                    href="!#"
                    data-abc="true"
                    className="nav-link widget-header"
                  >
                    {' '}
                    <i className="fas fa fa-whatsapp"></i>
                  </a>{' '}
                  <span className="vl"></span>
                  <div className="dropdown btn-group">
                    {' '}
                    <a
                      className="nav-link nav-icons"
                      href="!#"
                      id="navbarDropdownMenuLink1"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                      data-abc="true"
                    >
                      <i className="fas fa fa-bell"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-right notification-dropdown">
                      <li>
                        <div className="notification-title">More Info</div>
                        <div className="notification-list">
                          <div className="list-group">
                            {' '}
                            <a
                              href="affiliates"
                              className="list-group-item list-group-item-action active"
                              data-abc="true"
                            >
                              <div className="notification-info">
                                <div className="notification-list-user-img">
                                  <img
                                    src="https://img.icons8.com/nolan/100/000000/helping-hand.png"
                                    alt=""
                                    className="user-avatar-md rounded-circle"
                                  />
                                </div>
                                <div className="notification-list-user-block">
                                  <span className="notification-list-user-name">
                                    Affiliate program
                                  </span>{' '}
                                </div>
                              </div>
                            </a>{' '}
                            <a
                              href="redemption-center"
                              className="list-group-item list-group-item-action active"
                              data-abc="true"
                            >
                              <div className="notification-info">
                                <div className="notification-list-user-img">
                                  <img
                                    src="https://img.icons8.com/bubbles/100/000000/prize.png"
                                    alt=""
                                    className="user-avatar-md rounded-circle"
                                  />
                                </div>
                                <div className="notification-list-user-block">
                                  <span className="notification-list-user-name">
                                    Redemption Center
                                  </span>{' '}
                                </div>
                              </div>
                            </a>{' '}
                            <a
                              href="!#"
                              className="list-group-item list-group-item-action active"
                              data-abc="true"
                            >
                              <div className="notification-info">
                                <div className="notification-list-user-img">
                                  <img
                                    src="https://img.icons8.com/ultraviolet/100/000000/medal.png"
                                    alt=""
                                    className="user-avatar-md rounded-circle"
                                  />
                                </div>
                                <div className="notification-list-user-block">
                                  <span className="notification-list-user-name">
                                    Achievements
                                  </span>{' '}
                                </div>
                              </div>
                            </a>{' '}
                            <a
                              href="!#"
                              className="list-group-item list-group-item-action active"
                              data-abc="true"
                            >
                              <div className="notification-info">
                                <div className="notification-list-user-img">
                                  <img
                                    src="https://img.icons8.com/bubbles/100/000000/call-female.png"
                                    alt=""
                                    className="user-avatar-md rounded-circle"
                                  />
                                </div>
                                <div className="notification-list-user-block">
                                  <span className="notification-list-user-name">
                                    Contact us
                                  </span>{' '}
                                </div>
                              </div>
                            </a>{' '}
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>{' '}
                  <span className="vl"></span>{' '}
                  <a
                    className="nav-link nav-user-img"
                    href="!#"
                    data-bs-toggle="modal"
                    data-bs-target="#login-modal"
                    data-abc="true"
                  >
                    <span className="login">LOGIN</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </header>
    </>
  );
};

export default NavMenu;
