/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/self-closing-comp */
import React from 'react';
import { FaMapMarkerAlt, FaAngleRight, FaRedo } from 'react-icons/fa';
// import { MdOutlineFmdGood } from 'react-icons/md';

import './Home.css';
// import ProductBanner from './ProductBanner';

const Home = () => {
  return (
    <>
      {/* <section id="sidebar-1">
        <div>
          <h6 className="p-1 border-bottom">Home Furniture</h6>
          <ul>
            <li>
              <a href="!#">Living</a>
            </li>
            <li>
              <a href="!#">Dining</a>
            </li>
            <li>
              <a href="!#">Office</a>
            </li>
            <li>
              <a href="!#">Bedroom</a>
            </li>
            <li>
              <a href="!#">Kitchen</a>
            </li>
          </ul>
        </div>
        <div>
          <h6 className="p-1 border-bottom">Filter By</h6>
          <p className="mb-2">Color</p>
          <ul className="list-group">
            <li className="list-group-item list-group-item-action mb-2 rounded">
              <a href="!#">
                {' '}
                <span className="fa fa-circle pr-1" id="red"></span>Red{' '}
              </a>
            </li>
            <li className="list-group-item list-group-item-action mb-2 rounded">
              <a href="!#">
                {' '}
                <span className="fa fa-circle pr-1" id="teal"></span>Teal{' '}
              </a>
            </li>
            <li className="list-group-item list-group-item-action mb-2 rounded">
              <a href="!#">
                {' '}
                <span className="fa fa-circle pr-1" id="blue"></span>Blue{' '}
              </a>
            </li>
          </ul>
        </div>
        <div>
          <h6>Type</h6>
          <form className="ml-md-2">
            <div className="form-inline border rounded p-sm-2 my-2">
              {' '}
              <input type="radio" name="type" id="boring" />{' '}
              <label htmlFor="boring" className="pl-1 pt-sm-0 pt-1">
                Boring
              </label>{' '}
            </div>
            <div className="form-inline border rounded p-sm-2 my-2">
              {' '}
              <input type="radio" name="type" id="ugly" />{' '}
              <label htmlFor="ugly" className="pl-1 pt-sm-0 pt-1">
                Ugly
              </label>{' '}
            </div>
            <div className="form-inline border rounded p-md-2 p-sm-1">
              {' '}
              <input type="radio" name="type" id="notugly" />{' '}
              <label htmlFor="notugly" className="pl-1 pt-sm-0 pt-1">
                Not Ugly
              </label>{' '}
            </div>
          </form>
        </div>
      </section>
      <section id="products-1">
        <ProductBanner />
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-sm-4 col-11 offset-sm-0 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/963486/pexels-photo-963486.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Wooden chair with legs</p>
                  <p>$90</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
            <div className="col-lg-3 offset-lg-0 col-sm-4 offset-sm-2 col-11 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/1125137/pexels-photo-1125137.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Ugly chair and table set</p>
                  <p>$100</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-4 col-11 offset-sm-0 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/3757055/pexels-photo-3757055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Leather Lounger</p>
                  <p>$950</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-4 offset-lg-0 offset-sm-2 col-11 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.unsplash.com/photo-1537182534312-f945134cce34?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Tree Trunk table set</p>
                  <p>$390</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-3 col-sm-4 col-11 offset-sm-0 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/3230274/pexels-photo-3230274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Red Leather Bar Stool</p>
                  <p>$30</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-4 offset-lg-0 offset-sm-2 col-11 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/3773571/pexels-photo-3773571.png?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Modern Dining Table</p>
                  <p>$740</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-4 col-11 offset-sm-0 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/534172/pexels-photo-534172.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">Boring Dining Table</p>
                  <p>$760</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-4 offset-lg-0 offset-sm-2 col-11 offset-1">
              <div className="card">
                {' '}
                <img
                  className="card-img-top"
                  src="https://images.pexels.com/photos/37347/office-sitting-room-executive-sitting.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                  alt="Card cap"
                />
                <div className="card-body">
                  <p className="card-text">An Ugly Office</p>
                  <p>$90</p> <span className="fa fa-circle" id="red"></span>{' '}
                  <span className="fa fa-circle" id="teal"></span>{' '}
                  <span className="fa fa-circle" id="blue"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> */}

      {/* <!-- .block-slideshow --> */}
      {/* <div
        className="
            block-slideshow block-slideshow--layout--with-departments
            block
          "
      >
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-9 offset-lg-3">
              <div className="block-slideshow__body">
                <div className="owl-carousel">
                  <a className="block-slideshow__slide" href="!#">
                    <div
                      className="
                          block-slideshow__slide-image
                          block-slideshow__slide-image--desktop
                        "
                      style={{ backgroundImage: `url('images/slides/slide-1.jpg')` }}
                    ></div>
                    <div
                      className="
                          block-slideshow__slide-image
                          block-slideshow__slide-image--mobile
                        "
                      style={{ backgroundImage: 'url(images/slides/slide-1-mobile.jpg)' }}
                    ></div>
                    <div className="block-slideshow__slide-content">
                      <div className="block-slideshow__slide-title">
                        Big choice of
                        <br />
                        Plumbing products
                      </div>
                      <div className="block-slideshow__slide-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        <br />
                        Etiam pharetra laoreet dui quis molestie.
                      </div>
                      <div className="block-slideshow__slide-button">
                        <span className="btn btn-primary btn-lg">Shop Now</span>
                      </div>
                    </div>
                  </a>
                  <a className="block-slideshow__slide" href="!#">
                    <div
                      className="
                          block-slideshow__slide-image
                          block-slideshow__slide-image--desktop
                        "
                      style={{ backgroundImage: 'url(images/slides/slide-2.jpg)' }}
                    ></div>
                    <div
                      className="
                          block-slideshow__slide-image
                          block-slideshow__slide-image--mobile
                        "
                      style={{
                        backgroundImage: `url('images/slides/slide-2-mobile.jpg')`,
                      }}
                    ></div>
                    <div className="block-slideshow__slide-content">
                      <div className="block-slideshow__slide-title">
                        Screwdrivers
                        <br />
                        Professional Tools
                      </div>
                      <div className="block-slideshow__slide-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        <br />
                        Etiam pharetra laoreet dui quis molestie.
                      </div>
                      <div className="block-slideshow__slide-button">
                        <span className="btn btn-primary btn-lg">Shop Now</span>
                      </div>
                    </div>
                  </a>
                  <a className="block-slideshow__slide" href="!#">
                    <div
                      className="
                          block-slideshow__slide-image
                          block-slideshow__slide-image--desktop
                        "
                      style={{ backgroundImage: `url('images/slides/slide-3.jpg')` }}
                    ></div>
                    <div
                      className="
                          block-slideshow__slide-image
                          block-slideshow__slide-image--mobile
                        "
                      style={{
                        backgroundImage: `url('images/slides/slide-3-mobile.jpg')`,
                      }}
                    ></div>
                    <div className="block-slideshow__slide-content">
                      <div className="block-slideshow__slide-title">
                        One more
                        <br />
                        Unique header
                      </div>
                      <div className="block-slideshow__slide-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        <br />
                        Etiam pharetra laoreet dui quis molestie.
                      </div>
                      <div className="block-slideshow__slide-button">
                        <span className="btn btn-primary btn-lg">Shop Now</span>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      {/* <!-- .block-slideshow / end --> */}
      {/* <!-- Hero Section Begin --> */}
      <section className="hero mb-4">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3">
              <div className="hero__categories">
                <div className="hero__categories__all text-capitalize rounded-top">
                  {/* <i className="fa fa-bars" /> */}
                  <span className="text-dark fs-6">product categories</span>
                </div>
                <ul className="">
                  <li>
                    <a href="!#">
                      Fresh Meat
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                  <li>
                    <a href="!#">
                      Vegetables
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                  <li>
                    <a href="!#">
                      Fruit Nut Gifts
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                  <li>
                    <a href="!#">
                      Fresh Berries
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                  <li>
                    <a href="!#">
                      Ocean Foods
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                  <li>
                    <a href="!#">
                      Butter Eggs
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                  <li>
                    <a href="!#">
                      Fastfood
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <FaAngleRight className="text-muted" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-9 col-md-9 hero__banner py-4">
              <div
                className="hero__item set-bg"
                // data-setbg="/assets/images/gig_logo_3.png"
              >
                <div className="hero__text">
                  <span className="d-inline-block text-uppercase">grow your</span>
                  <h3 className="text-white">BUSINESS ONLINE</h3>
                  <p className="text-white">
                    You can reach over a million buyers across
                    <p>the country, Sign up as a store owner on GIG ALPHA today!</p>
                  </p>
                  <a href="!#" className="primary-btn btn-outline-white">
                    SHOP NOW
                  </a>
                </div>

                {/* Carousel */}
                {/* <div
                  id="mycarousel"
                  className="carousel slide"
                  data-bs-ride="carousel"
                  data-interval="2000"
                >
                  <ol className="carousel-indicators">
                    <li
                      data-bs-target="#mycarousel"
                      data-bs-slide-to="0"
                      className="active"
                    ></li>
                    <li data-bs-target="#mycarousel" data-bs-slide-to="1"></li>
                    <li data-bs-target="#mycarousel" data-bs-slide-to="2"></li>
                  </ol>
                  <div className="carousel-inner">
                    <div className="carousel-item active">
                      {' '}
                      <img
                        src="https://images.pexels.com/photos/462024/pexels-photo-462024.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                        className="d-block w-100"
                        alt="stuff"
                      />{' '}
                    </div>
                    <div className="carousel-item">
                      {' '}
                      <img
                        src="https://images.pexels.com/photos/258109/pexels-photo-258109.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                        className="d-block w-100"
                        alt="stuff"
                      />{' '}
                    </div>
                    <div className="carousel-item">
                      {' '}
                      <img src="/assets/images/hero_img.png" alt="hero-banner" />{' '}
                    </div>
                  </div>{' '}
                  <a
                    className="carousel-control-prev"
                    href="#mycarousel"
                    role="button"
                    data-bs-slide="prev"
                  >
                    <div className="banner-icons">
                      {' '}
                      <span className="fas fa-angle-left"></span>{' '}
                    </div>{' '}
                    <span className="sr-only">Previous</span>
                  </a>{' '}
                  <a
                    className="carousel-control-next"
                    href="#mycarousel"
                    role="button"
                    data-bs-slide="next"
                  >
                    <div className="banner-icons">
                      {' '}
                      <span className="fas fa-angle-right"></span>{' '}
                    </div>{' '}
                    <span className="sr-only">Next</span>
                  </a>
                </div> */}

                {/* Carousel End */}

                <div>
                  <img src="/assets/images/hero_img.png" alt="hero-banner" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="products-section mb-4">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-12 py-2 popular-near-you border">
              <div className="near-you mb-2">
                <FaMapMarkerAlt /> Popular Near you
              </div>
              <div className="d-flex">
                <div className="flex-shrink-0">
                  <img src="/assets/images/product_1.png" alt="product" />
                </div>
                <div className="flex-grow-1 ms-3">
                  <small className="text-muted">Ladies Sneakers</small>

                  <span className="d-block fs-6">N23,000</span>
                  <span className="d-block fs-6">
                    <FaMapMarkerAlt /> Lekki
                  </span>
                </div>
              </div>
              <hr />
              <div className="d-flex">
                <div className="flex-shrink-0">
                  <img src="/assets/images/product_2.png" alt="product" />
                </div>
                <div className="flex-grow-1 ms-3">
                  <small className="text-muted">Ladies Sneakers</small>

                  <span className="d-block fs-6">N23,000</span>
                  <span className="d-block fs-6">
                    <FaMapMarkerAlt /> Lekki
                  </span>
                </div>
              </div>
              <hr />
              <div className="d-flex">
                <div className="flex-shrink-0">
                  <img src="/assets/images/product_3.png" alt="product" />
                </div>
                <div className="flex-grow-1 ms-3">
                  <small className="text-muted">Ladies Sneakers</small>

                  <span className="d-block fs-6">N23,000</span>
                  <span className="d-block fs-6">
                    <FaMapMarkerAlt /> Lekki
                  </span>
                </div>
              </div>
              <hr />
              <div className="d-flex">
                <div className="flex-shrink-0">
                  <img src="/assets/images/product_1.png" alt="product" />
                </div>
                <div className="flex-grow-1 ms-3">
                  <small className="text-muted">Ladies Sneakers</small>

                  <span className="d-block fs-6">N23,000</span>
                  <span className="d-block fs-6">
                    <FaMapMarkerAlt /> Lekki
                  </span>
                </div>
              </div>
              <hr />
              <div className="d-flex">
                <div className="flex-shrink-0">
                  <img src="/assets/images/product_1.png" alt="product" />
                </div>
                <div className="flex-grow-1 ms-3">
                  <small className="text-muted">Ladies Sneakers</small>

                  <span className="d-block fs-6">N23,000</span>
                  <span className="d-block fs-6">
                    <FaMapMarkerAlt /> Lekki
                  </span>
                </div>
              </div>
              <hr />
              <div className="d-flex">
                <div className="flex-shrink-0">
                  <img src="/assets/images/product_3.png" alt="product" />
                </div>
                <div className="flex-grow-1 ms-3">
                  <small className="text-muted">Ladies Sneakers</small>

                  <span className="d-block fs-6">N23,000</span>
                  <span className="d-block fs-6">
                    <FaMapMarkerAlt /> Lekki
                  </span>
                </div>
              </div>
            </div>
            <div className="col-lg-9 col-md-9 col-sm-12 col-12">
              <div className="services border py-3 d-flex justify-content-around">
                <span className="text-capitalize d-inline-block pe-5 border-end">
                  {' '}
                  <img
                    src="/assets/images/truck.svg"
                    alt="truck"
                    className="img-fluid"
                  />{' '}
                  fast delivery
                </span>
                <span className="text-capitalize d-inline-block pe-5 border-end">
                  {' '}
                  <img
                    src="/assets/images/phone.svg"
                    alt="truck"
                    className="img-fluid"
                  />{' '}
                  customer support
                </span>
                <span className="text-capitalize d-inline-block pe-5 border-end">
                  {' '}
                  <img
                    src="/assets/images/card.svg"
                    alt="truck"
                    className="img-fluid"
                  />{' '}
                  seamless payment
                </span>
                <span className="text-capitalize d-inline-block px-2">
                  {' '}
                  <img
                    src="/assets/images/prices.svg"
                    alt="truck"
                    className="img-fluid"
                  />{' '}
                  best prices
                </span>
              </div>
              <h6 className="products-near-you-header px-2 py-3 my-3 text-dark">
                Products you may like
              </h6>
              <div className="row gy-4 gx-5 mb-4">
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>

                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                  <div className="card shadow" style={{ width: '13rem' }}>
                    <div className="position-absolute top-0 end-0 px-1">
                      <span className="fa fa-heart-o like" />
                    </div>
                    <img
                      src="https://images.pexels.com/photos/2536965/pexels-photo-2536965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
                      className="card-img-top img-fluid"
                      alt="product"
                    />
                    <div className="card-body">
                      <h5 className="card-title fs-14 text-muted">
                        Obsidian Wealth Bracelet
                      </h5>
                      <p className="card-text fs-6 fw-bold">N 1,320,300</p>
                      <p className="card-text fs-13 text-mute">Alpha code: 838901</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="app-section" className="mb-4">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-12 col-12 hidden"></div>
            <div
              className="col-lg-9 col-md-9 col-sm-12 col-12 app-section"
              style={{ backgroundImage: 'url(/assets/images/Rectangle_bg.png)' }}
            >
              <img
                src="/assets/images/shoe_grp.png"
                alt="shoes"
                className="img-fluid d-inline-block img-1 px-4"
              />
              <img
                src="/assets/images/stores.png"
                alt="stores"
                className="img-fluid d-inline-block img-2"
              />

              <span className="d-inline-block ps-4">
                <img src="/assets/images/app_text.png" alt="text" /> <br />
                <img src="/assets/images/app_text_1.png" alt="text" />
              </span>
              <span className="d-inline-block liner"></span>
              <span className="d-inline-block">download the app on</span>
            </div>
          </div>
        </div>
      </section>
      <section id="closest-to-you" className="mb-4">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-12 col-12 hidden"></div>
            <div className="col-lg-9 col-md-9 col-sm-12 col-12 closest-to-you-products">
              <h6 className="products-near-you-header px-2 py-3 my-3 text-dark">
                Shop by category
              </h6>
              <div className="row mt-5 mb-5">
                <div className="col-md-6">
                  <div className="card">
                    <div className="row g-0">
                      <div className="col-md-6">
                        <div className="d-flex flex-column justify-content-center">
                          <div className="main_image">
                            {' '}
                            <img
                              src="https://i.imgur.com/TAzli1U.jpg"
                              alt="product"
                              id="main_product_image"
                              width="350"
                            />{' '}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="p-3 right-side">
                          <div className="d-flex justify-content-between align-items-center">
                            <h6 className="text-dark heading fw-bold">
                              Beats by dre Headphone
                            </h6>
                          </div>
                          <p className="fs-15 fw-bold">&#8358;430.99</p>
                          <div className="content">
                            <p className="text-muted alpha-code">Alpha code: 838901</p>
                          </div>
                          <p className="fs-15">
                            <FaMapMarkerAlt /> Victoria Island
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 mb-4">
                  <div className="card">
                    <div className="row g-0">
                      <div className="col-md-6">
                        <div className="d-flex flex-column justify-content-center">
                          <div className="main_image">
                            {' '}
                            <img
                              src="https://i.imgur.com/TAzli1U.jpg"
                              alt="product"
                              id="main_product_image"
                              width="350"
                            />{' '}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="p-3 right-side">
                          <div className="d-flex justify-content-between align-items-center">
                            <h6 className="text-dark heading fw-bold">
                              Beats by dre Headphone
                            </h6>
                          </div>
                          <p className="fs-15 fw-bold"> &#8358;430.99</p>
                          <div className="content">
                            <p className="text-muted alpha-code">Alpha code: 838901</p>
                          </div>
                          <p className="fs-15">
                            <FaMapMarkerAlt /> Victoria Island
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card">
                    <div className="row g-0">
                      <div className="col-md-6">
                        <div className="d-flex flex-column justify-content-center">
                          <div className="main_image">
                            {' '}
                            <img
                              src="https://i.imgur.com/TAzli1U.jpg"
                              alt="product"
                              id="main_product_image"
                              width="350"
                            />{' '}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="p-3 right-side">
                          <div className="d-flex justify-content-between align-items-center">
                            <h6 className="text-dark heading fw-bold">
                              Beats by dre Headphone
                            </h6>
                          </div>
                          <p className="fs-15 fw-bold">&#8358;430.99</p>
                          <div className="content">
                            <p className="text-muted alpha-code">Alpha code: 838901</p>
                          </div>
                          <p className="fs-15">
                            <FaMapMarkerAlt /> Victoria Island
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 mb-4">
                  <div className="card">
                    <div className="row g-0">
                      <div className="col-md-6">
                        <div className="d-flex flex-column justify-content-center">
                          <div className="main_image">
                            {' '}
                            <img
                              src="https://i.imgur.com/TAzli1U.jpg"
                              alt="product"
                              id="main_product_image"
                              width="350"
                            />{' '}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="p-3 right-side">
                          <div className="d-flex justify-content-between align-items-center">
                            <h6 className="text-dark heading fw-bold">
                              Beats by dre Headphone
                            </h6>
                          </div>
                          <p className="fs-15 fw-bold">&#8358;30.99</p>
                          <div className="content">
                            <p className="text-muted alpha-code">Alpha code: 838901</p>
                          </div>
                          <p className="fs-15">
                            <FaMapMarkerAlt /> Victoria Island
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-12 load-more text-center py-2 fs-15 ">
                  <a href="!#" className="text-decoration-none text-muted">
                    <FaRedo /> load more
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
