/* eslint-disable react/forbid-prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-use-before-define */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable func-names */
/* eslint-disable react/self-closing-comp */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import React, { useEffect, useState, useRef } from 'react';
import { useAlert } from 'react-alert';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { orderShipmentLogin } from '../../actions/orderActions';
import {
  getProductDetails,
  clearErrors,
  getProducts,
} from '../../actions/productActions';
// import { addItemToCart } from '../../actions/cartActions';
import MetaData from '../shared/MetaData';
import Loader from '../shared/Loader';
import Modal from '../shared/Modal';
import Shipping from '../cart/Shipping';
import ShipmentOption from '../cart/ShipmentOption';
import ColorPicker from '../shared/ColorPicker';
import PickupStations from '../cart/PickupStations';
import MyGoogleMaps from '../maps/MyGoogleMaps';

// import { NEW_REVIEW_RESET } from '../../actions/actionTypes';
// import ListReviews from '../review/ListReviews';

const ProductDetails = ({ match }) => {
  const [show, showModal] = useState(false);

  const [quantity, setQuantity] = useState(1);
  const [size, setSize] = useState(null);
  const [colorPicked, setColorPicked] = useState('');
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState('');
  const dispatch = useDispatch();
  const { loading, error, product } = useSelector((state) => state.productDetails);
  const {
    loading: loader,
    products: allProducts,
    error: productError,
  } = useSelector((state) => state.products);

  const { status, error: orderLoginError } = useSelector((state) => state.shipping);

  const { user } = useSelector((state) => state.auth);

  const alert = useAlert();
  useEffect(() => {
    dispatch(getProductDetails(match.params.id));
    if (error) {
      dispatch(clearErrors());
    }
    dispatch(orderShipmentLogin());
    if (orderLoginError) {
      alert.error(error);
      dispatch(clearErrors());
    }
  }, [dispatch, alert, error, match.params.id]);

  const productList = allProducts;
  console.log(product, ' the single product');
  const orderLoginStatus = status && status;
  console.log(orderLoginStatus);

  const productId = window.location.pathname.slice(9);
  console.log(productId, ' the product pathname');
  const productDetails = product && product;
  console.log(productDetails);
  // console.log(productDetails && productDetails.productName);

  const rightView = productDetails && productDetails.right_view;
  const leftView = productDetails && productDetails.left_view;
  const rearView = productDetails && productDetails.rear_view;

  const [selectedImg, setSelectedImg] = useState(
    productDetails && productDetails.front_view
  );
  // console.log(selectedImg);

  // Calculate Order Prices
  const productPrice = productDetails && productDetails.price;
  const itemsPrice = quantity * productPrice;

  const colors = productDetails && productDetails.color;
  const productSize = productDetails && productDetails.size;
  console.log(productSize);

  // const shippingPrice = itemsPrice > 200 ? 0 : 25;

  // const totalPrice = (itemsPrice + shippingPrice).toFixed(2);
  localStorage.setItem('productPrice', JSON.stringify(productPrice));
  localStorage.setItem('orderQty', JSON.stringify(quantity));
  localStorage.setItem('size', JSON.stringify(size));
  localStorage.setItem('colorPicked', JSON.stringify(colorPicked));

  const increaseQty = () => {
    const count = document.querySelector('.count');

    if (count.valueAsNumber >= product.quantity) return;
    // If not, increase the quantity and update the state.
    const qty = count.valueAsNumber + 1;
    setQuantity(qty);
  };

  const decreaseQty = () => {
    const count = document.querySelector('.count');

    if (count.valueAsNumber <= 1) return;
    // If qty not less than 1, decreaseQty in the cart
    const qty = count.valueAsNumber - 1;
    setQuantity(qty);
  };

  function setUserRatings() {
    const stars = document.querySelectorAll('.star');

    stars.forEach((star, index) => {
      star.starValue = index + 1;

      ['click', 'mouseover', 'mouseout'].forEach(function (e) {
        star.addEventListener(e, showRatings);
      });
    });

    function showRatings(e) {
      stars.forEach((star, index) => {
        if (e.type === 'click') {
          if (index < this.starValue) {
            star.classList.add('orange');

            setRating(this.starValue);
          } else {
            star.classList.remove('orange');
          }
        }

        if (e.type === 'mouseover') {
          if (index < this.starValue) {
            star.classList.add('yellow');
          } else {
            star.classList.remove('yellow');
          }
        }

        if (e.type === 'mouseout') {
          star.classList.remove('yellow');
        }
      });
    }
  }

  const handleCheckBoxChange = (e) => {
    console.log(e.target.value);
    setSize(Number(e.target.value));
  };

  const handleChangeComplete = (color, event) => {
    setColorPicked(color.hex);
  };

  const myDisabledBtn = useRef(null);

  const alertUser = () => {
    if (myDisabledBtn.current.focused()) {
      alert('I have been clicked!');
    }
    // myDisabledBtn.current.disabled.alert('I have been clicked!');
    // document.querySelector('#my-alert-button').addEventListener('click', function () {
    //   alert('Clicked on button!');
    // });
  };

  // function clickMe(data) {
  //   data.classList.add('checked');
  //   console.log('I have been clicked!');
  // }

  console.log(productDetails, ' this is the props');
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <MetaData title={productDetails && productDetails.productName} />
          <div className="container">
            <div className="row d-flex justify-content-around bg-white rounded-3 my-5 shadow pb-5">
              <div className="col-12 col-lg-4">
                <div
                  className="rounded-4 product-image border-lg border-lg-1 mt-5 h-75 shadow-sm"
                  id="product_image"
                >
                  {!loading && productDetails && productDetails.productName && (
                    <img
                      className="d-block mw-100 h-100 mx-auto py-4"
                      // src={productDetails && productDetails.front_view}
                      src={
                        selectedImg === undefined
                          ? productDetails && productDetails.front_view
                          : selectedImg
                      }
                      alt={productDetails && productDetails.productName}
                    />
                  )}
                </div>
                <div className="d-flex flex-row justify-content-around mt-1 img-views">
                  <span className="border border-2 shadow-sm rounded-3">
                    {productDetails && productDetails.front_view && (
                      <img
                        className="mx-auto w-30"
                        src={productDetails && productDetails.front_view}
                        alt={productDetails && productDetails.productName}
                        onClick={() =>
                          setSelectedImg(productDetails && productDetails.front_view)
                        }
                      />
                    )}
                  </span>
                  <span className="border border-2 shadow-sm rounded-3">
                    {productDetails && productDetails.right_view && (
                      <img
                        className="mx-auto w-30"
                        src={productDetails && productDetails.right_view}
                        alt={productDetails && productDetails.productName}
                        onClick={() => setSelectedImg(rightView)}
                      />
                    )}
                  </span>
                  <span className="border border-2 shadow-sm rounded-3">
                    {productDetails && productDetails.left_view && (
                      <img
                        className="mx-auto w-30"
                        src={productDetails && productDetails.left_view}
                        alt={productDetails && productDetails.productName}
                        onClick={() => setSelectedImg(leftView)}
                      />
                    )}
                  </span>
                  <span className="border border-2 shadow-sm rounded-3">
                    {productDetails && productDetails.rear_view && (
                      <img
                        className="mx-auto w-30"
                        src={productDetails && productDetails.rear_view}
                        alt={productDetails && productDetails.productName}
                        onClick={() => setSelectedImg(rearView)}
                      />
                    )}
                  </span>
                </div>
              </div>

              <div className="col-12 col-lg-6 mt-5">
                <h3 className="h2" id="product_name">
                  {productDetails && productDetails.productName}
                </h3>
                {/* <p id="product_id">Product # {productDetails[0]._id}</p> */}
                {/* Orders/reviews */}

                {/* Users/store */}
                <div className="user-store d-flex flex-row">
                  <div className="brand me-2">
                    <img src="/assets/images/default_avatar.jpg" alt="avatar" />
                  </div>
                  <div className="store-name d-flex flex-column align-self-center">
                    <h6 className="text-muted">Store name</h6>
                    <p>{productDetails && productDetails?.store?.name}</p>
                  </div>
                </div>
                <hr />

                {/* Price/quantity */}
                <div className="d-flex justify-content-between price-quantity">
                  <p id="product_price" className="fs-2">
                    &#8358;{productDetails && productDetails.price}
                  </p>
                  <div className="stockCounter d-inline">
                    <span className="me-3">Qty .</span>

                    <input
                      type="button"
                      value="-"
                      className="minus btn-success-1 text-white fw-bold py-2 rounded-start"
                      onClick={decreaseQty}
                    />
                    <input
                      type="number"
                      value={quantity}
                      readOnly
                      className="count text bg-light d-inline fw-bold py-2 text-dark"
                    />
                    <input
                      type="button"
                      value="+"
                      className="plus btn-success-1 text-white fw-bold py-2 rounded-end"
                      onClick={increaseQty}
                    />
                  </div>
                </div>
                <div className="color-choice-wrapper d-flex justify-content-between">
                  {/* Status */}
                  <p>
                    Status:{' '}
                    <span
                      id="stock_status"
                      className={
                        productDetails && productDetails.quantity > 0
                          ? 'greenColor'
                          : 'redColor'
                      }
                    >
                      {productDetails && productDetails.quantity > 0
                        ? 'In Stock'
                        : 'Out of Stock'}
                    </span>
                  </p>
                  {/* colors */}
                  {/* <div className="d-flex justify-content-end">
                    <p className="me-2">Color</p>
                    <div className="color-choice d-flex justify-content-around">
                      <div className="color-box color-1"></div>
                      <div className="color-box color-2"></div>
                      <div className="color-box color-3"></div>
                      <div className="color-box color-4"></div>
                    </div>
                  </div> */}
                </div>
                <hr />

                <div className="product-sizes mb-3">
                  <p>Choose preferred color</p>
                  <ColorPicker
                    className="unchecked"
                    color={colors}
                    handleChangeComplete={handleChangeComplete}
                  />
                </div>
                {/* Product Sizes */}
                <div className="product-sizes mb-3">
                  <p>Sizes available</p>
                  {/* <p>{size}</p> */}
                  {/* <p>{typeof size}</p> */}
                  {/* <p>{colorPicked}</p> */}
                  <div className="">
                    {/* <span className="badge bg-light text-muted">45</span>
                    <span className="badge bg-light text-muted">72</span>
                    <span className="badge bg-light text-muted">53</span>
                    <span className="badge bg-light text-muted">23</span>
                    <span className="badge bg-light text-muted">12</span>
                    <span className="badge bg-light text-muted">12</span>
                    <span className="badge bg-light text-muted">14</span>
                    <span className="badge bg-light text-muted">56</span> */}
                    {productSize &&
                      productSize.map((item) => (
                        <div
                          key={item._id}
                          className="btn-group btn-group-sm"
                          role="group"
                          aria-label="Basic checkbox toggle button group"
                        >
                          <input
                            type="checkbox"
                            className="btn-check me-1"
                            id={item._id}
                            value={item.value.toString()}
                            onChange={handleCheckBoxChange}
                          />
                          <label className="btn btn-light" htmlFor={item._id}>
                            {item.value}
                          </label>

                          {/* <input type="checkbox" className="btn-check" id="btncheck2" />
                          <label className="btn btn-light" htmlFor="btncheck2">
                            45
                          </label>

                          <input type="checkbox" className="btn-check" id="btncheck3" />
                          <label className="btn btn-light" htmlFor="btncheck3">
                            23
                          </label>

                          <input type="checkbox" className="btn-check" id="btncheck1" />
                          <label className="btn btn-light" htmlFor="btncheck1">
                            49
                          </label> */}
                        </div>
                      ))}
                  </div>
                </div>
                {/* Checkout btn */}
                <div className="d-grid gap-2 btn-success-1">
                  <button
                    ref={myDisabledBtn}
                    id="my-alert-button"
                    type="button"
                    className="btn text-uppercase fw-bold"
                    disabled={
                      !(size && colorPicked) ||
                      (productDetails && productDetails.quantity === 0)
                    }
                    onClick={() => {
                      showModal(true);
                    }}
                  >
                    checkout
                  </button>
                </div>
                <Modal show={show} handleClose={() => showModal(false)}>
                  {/* <Shipping
                    qty={quantity}
                    productPrice={productDetails && productDetails.price}
                  /> */}
                  {/* <ShipmentOption /> */}
                  <PickupStations productId={match.params.id} />
                </Modal>
              </div>
            </div>

            {/* Reviews */}

            {/* <div className="row d-flex justify-content-around mb-5">
              <section id="reviews" className="col-lg-8 bg-white rounded-3 shadow px-1">
                <div className="text-start py-2">
                  <h4 className="text-uppercase text-dark fs-5">
                    Review for this item{' '}
                    <span className="d-inline-block py-1 px-1 rounded-pill text-center my-auto">
                      5
                    </span>
                  </h4>
                </div>

                <div className="">
                  <div className="list-group">
                    <div className="list-group-item py-3">
                      <div className="pb-2">
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star text-warning"></i>
                      </div>
                      <h5 className="mb-1">A must-buy for every aspiring web dev</h5>
                      <p className="mb-1">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
                        error veniam sit expedita est illo maiores neque quos nesciunt,
                        reprehenderit autem odio commodi labore praesentium voluptate
                        repellat in id quisquam.
                      </p>
                      <small>Review by Mario</small>
                    </div>
                    <div className="list-group-item py-3">
                      <div className="pb-2">
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star text-warning"></i>
                      </div>
                      <h5 className="mb-1">A must-buy for every aspiring web dev</h5>
                      <p className="mb-1">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
                        error veniam sit expedita est illo maiores neque quos nesciunt,
                        reprehenderit autem odio commodi labore praesentium voluptate
                        repellat in id quisquam.
                      </p>
                      <small>Review by Mario</small>
                    </div>
                    <div className="list-group-item py-3">
                      <div className="pb-2">
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star text-warning"></i>
                      </div>
                      <h5 className="mb-1">A must-buy for every aspiring web dev</h5>
                      <p className="mb-1">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
                        error veniam sit expedita est illo maiores neque quos nesciunt,
                        reprehenderit autem odio commodi labore praesentium voluptate
                        repellat in id quisquam.
                      </p>
                      <small>Review by Mario</small>
                    </div>
                    <div className="list-group-item py-3">
                      <div className="pb-2">
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star text-warning"></i>
                      </div>
                      <h5 className="mb-1">A must-buy for every aspiring web dev</h5>
                      <p className="mb-1">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
                        error veniam sit expedita est illo maiores neque quos nesciunt,
                        reprehenderit autem odio commodi labore praesentium voluptate
                        repellat in id quisquam.
                      </p>
                      <small>Review by Mario</small>
                    </div>
                    <div className="list-group-item py-3">
                      <div className="pb-2">
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star-fill text-warning"></i>
                        <i className="bi bi-star text-warning"></i>
                      </div>
                      <h5 className="mb-1">A must-buy for every aspiring web dev</h5>
                      <p className="mb-1">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
                        error veniam sit expedita est illo maiores neque quos nesciunt,
                        reprehenderit autem odio commodi labore praesentium voluptate
                        repellat in id quisquam.
                      </p>
                      <small>Review by Mario</small>
                    </div>
                  </div>
                </div>
              </section>
              <section className="col-lg-4 py-2" id="product-specifications">
                <div className="card px-2">
                  <div className="card-header text-uppercase h4 my-2">specifications</div>
                  <ul className="list-group">
                    <li className="list-group-item d-flex justify-content-between align-items-start py-2">
                      <div className="ms-2 me-auto">
                        <div className="text-muted">Model</div>
                      </div>
                      <span className="fw-bold">Odsy-1000</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-start bg-light py-1">
                      <div className="ms-2 me-auto">
                        <div className="text-muted">Color</div>
                      </div>

                      <div className="dropdown">
                        <button
                          className="btn btn-light btn-sm dropdown-toggle"
                          type="button"
                          id="dropdownMenuButton1"
                          data-bs-toggle="dropdown"
                          aria-expanded="false"
                        >
                          Teal
                        </button>
                        <ul
                          className="dropdown-menu"
                          aria-labelledby="dropdownMenuButton1"
                        >
                          <li>
                            <a className="dropdown-item" href="!#">
                              green
                            </a>
                          </li>
                          <li>
                            <a className="dropdown-item" href="!#">
                              yellow
                            </a>
                          </li>
                          <li>
                            <a className="dropdown-item" href="!#">
                              red
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-start py-2">
                      <div className="ms-2 me-auto">
                        <div className="text-muted">Product Location</div>
                      </div>
                      <span className="fw-bold">Lekki</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-start bg-light py-2">
                      <div className="ms-2 me-auto">
                        <div className="text-muted h6">Estimated delivery time</div>
                      </div>
                      <small className="fw-bold text-success">2 Hours</small>
                    </li>
                  </ul>
                  <div className="return-policies my-2">
                    <h6 className="bg-light p-1 mx-auto">Shipping and return policies</h6>
                    <div className="d-flex justify-content-between">
                      <span>Ready to ship in</span>
                      <span>Cost to ship</span>
                    </div>
                    <div className="d-flex justify-content-between text-success">
                      <span>4 business days</span>
                      <span>N2,100</span>
                    </div>
                    <div className="d-flex justify-content-between">
                      <span>Returns & exchanges</span>
                    </div>
                    <div className="d-flex justify-content-between">
                      <p>
                        <span className="text-success">Accepted</span>{' '}
                        <small>(Exceptions may apply)</small>
                      </p>
                    </div>
                  </div>
                </div>
              </section>
            </div> */}
            {/* <MyGoogleMaps /> */}
          </div>
        </>
      )}
    </>
  );
};

// ProductDetails.propTypes = {
//   match: PropTypes.object.isRequired,
// };

ProductDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default ProductDetails;
