/* eslint-disable react/forbid-prop-types */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Products = ({ product }) => {
  const handleNavigation = () => {
    window.location.href = `/product/${product._id}`;
  };
  return (
    <div className="col-sm-12 col-md-6 my-3">
      <div className="card p-3 rounded">
        <img
          className="card-img-top mx-auto"
          src={product.front_view}
          alt={product.productName}
        />
        <div className="card-body d-flex flex-column">
          <h5 className="card-title">
            <Link to={`/product/${product._id}`}>{product.productName}</Link>
          </h5>
          <div className="ratings mt-auto">
            <div className="rating-outer">
              <div
                className="rating-inner"
                style={{ width: `${(product.ratings / 5) * 100}%` }}
              />
            </div>
            <span id="no_of_reviews">({product.ratings_reviews} Reviews)</span>
          </div>
          <p className="card-text">${product.price}</p>
          <button
            type="button"
            onClick={handleNavigation}
            id="view_btn"
            className="btn btn-block"
          >
            View Details
          </button>
        </div>
      </div>
    </div>
  );
};

Products.propTypes = {
  product: PropTypes.object.isRequired,
};

export default Products;
