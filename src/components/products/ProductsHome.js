/* eslint-disable consistent-return */
/* eslint-disable no-underscore-dangle */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import { getProducts } from '../../actions/productActions';
import MetaData from '../shared/MetaData';
import Loader from '../shared/Loader';
import ProductHeader from './ProductHeader';
import Products from './Products';

const ProductsHome = () => {
  const dispatch = useDispatch();
  const alert = useAlert();
  // Map redux state to props of Home component
  const { loading, products, error } = useSelector((state) => state.products);

  useEffect(() => {
    if (error) {
      // alert.success('success');
      return alert.error(error);
    }
    dispatch(getProducts());
  }, [dispatch, alert, error]);

  return (
    <div>
      {loading ? (
        <Loader />
      ) : (
        <>
          {' '}
          <MetaData title="Buy affordable and quality products online" />
          <main className="main" id="top">
            {/* Product Display */}
            <section className="py-0">
              <div className="container">
                <div className="row h-100">
                  {/* Header */}
                  <ProductHeader title="popular deals" />

                  <div className="col-12">
                    <div className="row h-100 align-items-center g-2">
                      {products &&
                        products.map((product) => (
                          <Products key={product._id} product={product} />
                        ))}
                    </div>
                  </div>
                </div>
              </div>
            </section>
            {/* <div className="d-flex justify-content-center mt-5">
              {resPerPage <= count && (
                <Pagination
                  activePage={currentPage} //current page where the user is.
                  itemsCountPerPage={resPerPage}
                  totalItemsCount={productsCount}
                  // pageRangeDisplayed={5}
                  onChange={handlePageChangeNo}
                  nextPageText={'Next'}
                  prevPageText={'Prev'}
                  firstPageText={'First'}
                  lastPageText={'Last'}
                  itemClass="page-item" //bs classes
                  linkClass="page-link"
                />
              )}
            </div> */}
          </main>
        </>
      )}
    </div>
  );
};

export default ProductsHome;
