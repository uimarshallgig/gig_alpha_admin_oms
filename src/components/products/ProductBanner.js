/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';

const ProductBanner = () => {
  const slides = document.querySelectorAll('.slide-container');
  let index = 0;

  const next = () => {
    slides[index].classList.remove('active');
    index = (index + 1) % slides.length;
    slides[index].classList.add('active');
  };

  const prev = () => {
    slides[index].classList.remove('active');
    index = (index - 1 + slides.length) % slides.length;
    slides[index].classList.add('active');
  };
  return (
    <>
      {/* <!-- home section starts  --> */}

      <section className="home" id="home">
        <div className="slide-container active">
          <div className="slide">
            <div className="content">
              <span>nike red shoes</span>
              <h3>nike metcon shoes</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat maiores
                et eos eaque veritatis aut laboriosam earum dolorem iste atque.
              </p>
              <a href="!#" className="btn">
                add to cart
              </a>
            </div>
            <div className="image">
              <img src="/assets/images/home-shoe-1.png" className="shoe" alt="shoe" />
              {/* <img src="images/home-text-1.png" className="text" alt="" /> */}
            </div>
          </div>
        </div>

        <div className="slide-container">
          <div className="slide">
            <div className="content">
              <span>nike blue shoes</span>
              <h3>nike metcon shoes</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat maiores
                et eos eaque veritatis aut laboriosam earum dolorem iste atque.
              </p>
              <a href="!#" className="btn">
                add to cart
              </a>
            </div>
            <div className="image">
              <img src="/assets/images/home-shoe-2.png" className="shoe" alt="shoe" />
              {/* <img src="images/home-text-2.png" className="text" alt="" /> */}
            </div>
          </div>
        </div>

        <div className="slide-container">
          <div className="slide">
            <div className="content">
              <span>nike yellow shoes</span>
              <h3>nike metcon shoes</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat maiores
                et eos eaque veritatis aut laboriosam earum dolorem iste atque.
              </p>
              <a href="!#" className="btn">
                add to cart
              </a>
            </div>
            <div className="image">
              <img src="/assets/images/home-shoe-3.png" className="shoe" alt="shoe" />
              {/* <img src="images/home-text-3.png" className="text" alt="" /> */}
            </div>
          </div>
        </div>

        <div id="prev" className="fas fa-chevron-left" onClick={prev} />
        <div id="next" className="fas fa-chevron-right" onClick={next} />
      </section>

      {/* <!-- home section ends --> */}
    </>
  );
};

export default ProductBanner;
