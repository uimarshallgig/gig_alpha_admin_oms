/* eslint-disable react/jsx-props-no-spreading */
import { PureComponent } from 'react';

function withNavBar(Component) {
  class WithNavBar extends PureComponent {
    render() {
      return (
        <>
          {/* <NavBar /> */}
          <Component {...this.props} />
        </>
      );
    }
  }

  return WithNavBar;
}

export default withNavBar;

// Usage
// ==================================
// export default withNavBar(Dashboard);
// export default withNavBar(Home);
