/* eslint-disable react/jsx-boolean-value */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
import React, { Suspense, lazy } from 'react';
import { Route, Switch } from 'react-router-dom';

import Spinner from 'react-bootstrap/Spinner';

// import ProtectedRoute from './ProtectedRoute';

const Dashboard = lazy(() => import('../dashboard/Dashboard'));
const LogIn = lazy(() => import('../auth/LogIn'));
const Register = lazy(() => import('../auth/Register'));
const ProductsHome = lazy(() => import('../products/ProductsHome'));
const Home = lazy(() => import('../products/Home'));

const ProductDetails = lazy(() => import('../products/ProductDetails'));
const Shipping = lazy(() => import('../cart/Shipping'));
const ConfirmOrder = lazy(() => import('../cart/ConfirmOrder'));
const Payment = lazy(() => import('../cart/Payment'));
const OrderSuccess = lazy(() => import('../cart/OrderSuccess'));
const OrderFailure = lazy(() => import('../cart/OrderFailure'));
const SelectPickupLocation = lazy(() => import('../cart/SelectPickupLocation'));
const SelectedLocation = lazy(() => import('../cart/SelectedLocation'));
const DeliveryDetails = lazy(() => import('../cart/DeliveryDetails'));
const DeliveryAddress = lazy(() => import('../cart/DeliveryAddress'));
const PaymentDetails = lazy(() => import('../cart/PaymentDetails'));

const Routes = () => {
  return (
    <>
      <Suspense
        fallback={
          <Spinner animation="border" variant="warning" className="spinner-inner" />
        }
      >
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/login" component={LogIn} />
          <Route path="/register" component={Register} />
          <Route path="/products" component={ProductsHome} />
          <Route path="/product-home" component={Home} />
          <Route
            path="/product/:id"
            render={(props) => (
              <ProductDetails {...props} url="https://picsum.photos/200" />
            )}
            exact
          />
          <Route path="/pickup-stations" component={SelectPickupLocation} exact />
          <Route path="/selected-location" component={SelectedLocation} exact />
          <Route path="/delivery-details" component={DeliveryDetails} exact />
          <Route path="/delivery-address" component={DeliveryAddress} exact />
          <Route path="/payment-details" component={PaymentDetails} exact />
          <Route path="/shipping" component={Shipping} exact />
          <Route path="/order/confirm" component={ConfirmOrder} exact />
          <Route path="/payment" component={Payment} />
          <Route path="/success" component={OrderSuccess} />
          <Route path="/failure" component={OrderFailure} />
          {/* <ProtectedRoute path="/dashboard" isAdmin={true} component={Dashboard} exact /> */}
        </Switch>
      </Suspense>
    </>
  );
};

export default Routes;
