/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link, useHistory } from 'react-router-dom';

import { useAlert } from 'react-alert';
import { useDispatch, useSelector } from 'react-redux';

import './LogIn.css';
import Loader from '../shared/Loader';
import MetaData from '../shared/MetaData';
import { loginUser, clearErrors } from '../../actions/userActions';

function LogIn() {
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const alert = useAlert();
  const dispatch = useDispatch();

  const { isAuthenticated, error, loading } = useSelector((state) => state.auth);

  useEffect(() => {
    if (isAuthenticated) {
      history.push('/');
    }

    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }
  }, [dispatch, alert, isAuthenticated, error, history]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(loginUser(email, password));
  };

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <MetaData title="Login" />
          <div className="my-login-page mt-5">
            <section className="h-100">
              <div className="container h-100">
                <div className="row justify-content-md-center h-100">
                  <div className="card-wrapper">
                    <div className="brand">
                      <img src="assets/images/default_avatar.jpg" alt="avatar" />
                    </div>
                    <div className="card fat">
                      <div className="card-body">
                        <h4 className="card-title">Login</h4>
                        <form onSubmit={handleSubmit} className="my-login-validation">
                          <div className="form-group my-2">
                            <label htmlFor="email">E-Mail Address</label>
                            <input
                              id="email"
                              type="email"
                              className="form-control"
                              name="email"
                              value={email}
                              onChange={(e) => setEmail(e.target.value)}
                              required
                            />
                          </div>

                          <div className="form-group my-3">
                            <label htmlFor="password">
                              Password
                              <Link
                                to="/password/forgot"
                                className="float-end link-info text-decoration-none"
                              >
                                Forgot Password?
                              </Link>
                            </label>
                            <input
                              id="password"
                              type="password"
                              className="form-control"
                              name="password"
                              required
                              value={password}
                              onChange={(e) => setPassword(e.target.value)}
                            />
                          </div>

                          {/* <div className="form-group">
                            <div className="custom-checkbox custom-control">
                              <input
                                type="checkbox"
                                name="remember"
                                id="remember"
                                className="custom-control-input"
                              />
                              <label htmlFor="remember" className="custom-control-label">
                                Remember Me
                              </label>
                            </div>
                          </div> */}

                          <div className="form-group m-0 d-grid">
                            <button type="submit" className="btn btn-primary d-block">
                              Login
                            </button>
                          </div>
                          {/* <div className="mt-4 text-center">
                            Don&apos;t have an account?
                            <Link className="text-info" to="/register">
                              Create One
                            </Link>
                          </div> */}
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </>
      )}
    </>
    // <>
    //   {loading ? (
    //     <Loader />
    //   ) : (
    //     <>
    //       <MetaData title="Login" />
    //       <div className="container-fluid">
    //         <div className="row no-gutter">
    //           <div className="d-none d-md-flex col-md-4 col-lg-6 bg-image" />
    //           <div className="col-md-8 col-lg-6">
    //             <div className="login d-flex align-items-center py-5">
    //               <div className="container">
    //                 <div className="row">
    //                   <div className="col-md-9 col-lg-8 mx-auto">
    //                     <h3 className="login-heading mb-4">Welcome back!</h3>

    //                     <form onSubmit={handleSubmit}>
    //                       <div className="form-label-group">
    //                         <input
    //                           type="email"
    //                           className="form-control"
    //                           name="email"
    //                           value={email}
    //                           onChange={(e) => setEmail(e.target.value)}
    //                           placeholder="Email address"
    //                           autoComplete="email"
    //                           id="inputEmail"
    //                           required
    //                         />
    //                         <label htmlFor="inputEmail">Email address</label>
    //                       </div>

    //                       <div className="form-label-group">
    //                         <input
    //                           id="inputPassword"
    //                           type="password"
    //                           className="form-control"
    //                           name="password"
    //                           value={password}
    //                           onChange={(e) => setPassword(e.target.value)}
    //                           autoComplete="password"
    //                           required
    //                         />
    //                         <label htmlFor="inputPassword">Password</label>
    //                       </div>

    //                       <button
    //                         className="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2"
    //                         type="submit"
    //                       >
    //                         Sign in
    //                       </button>
    //                       <div className="text-center">
    //                         <Link to="/password/forgot" className="small">
    //                           Forgot Password?
    //                         </Link>
    //                       </div>
    //                     </form>
    //                   </div>
    //                 </div>
    //               </div>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </>
    //   )}
    // </>
  );
}

LogIn.propTypes = {};

export default LogIn;
