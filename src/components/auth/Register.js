/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-unneeded-ternary */
import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useAlert } from 'react-alert';
import { useDispatch, useSelector } from 'react-redux';
import { registerUser, clearErrors, getUserRoles } from '../../actions/userActions';

import MetaData from '../shared/MetaData';
import './LogIn.css';

const Register = () => {
  const history = useHistory();

  const { isAuthenticated, error, loading } = useSelector((state) => state.auth);

  const { userRoles } = useSelector((state) => state.role);
  // console.log(userRoles);

  const [roles, setRoles] = useState(userRoles);
  const [user, setUser] = useState({
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    phone: '',
  });

  const { first_name, last_name, email, password, phone } = user;

  const alert = useAlert();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserRoles());
    if (isAuthenticated) {
      history.push('/');
    }

    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }
  }, [dispatch, alert, isAuthenticated, error, history]);

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.set('first_name', first_name);
    formData.set('last_name', last_name);
    formData.set('email', email);
    formData.set('password', password);
    formData.set('phone', phone);
    formData.set('roles', roles);

    dispatch(registerUser(formData));
  };

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
    setRoles({ ...roles, [e.target.name]: e.target.value });
  };
  return (
    <>
      <MetaData title="Register User" />
      <div className="my-login-page">
        <section className="h-100">
          <div className="container h-100">
            <div className="row justify-content-md-center h-100">
              <div className="card-wrapper">
                <div className="brand">
                  <img src="assets/img/default_avatar.jpg" alt="avatar" />
                </div>
                <div className="card fat">
                  <div className="card-body">
                    <h4 className="card-title">Register</h4>
                    <form onSubmit={handleSubmit} className="my-login-validation">
                      <div className="form-group">
                        <label htmlFor="fname">First Name</label>
                        <input
                          id="fname"
                          type="text"
                          className="form-control"
                          name="fname"
                          value={first_name}
                          onChange={handleChange}
                          required
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="lname">Last Name</label>
                        <input
                          id="lname"
                          type="text"
                          className="form-control"
                          name="lname"
                          value={last_name}
                          onChange={handleChange}
                          required
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="email">E-Mail Address</label>
                        <input
                          id="email"
                          type="email"
                          className="form-control"
                          name="email"
                          value={email}
                          onChange={handleChange}
                          required
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="phone_field">Phone No</label>
                        <input
                          type="phone"
                          id="phone_field"
                          className="form-control"
                          value={phone}
                          onChange={handleChange}
                          required
                        />
                      </div>

                      {/* <div className="form-group">
                        <label htmlFor="roles_field">Roles</label>
                        <select
                          id="roles_field"
                          className="form-control"
                          value={roles}
                          onChange={(e) => setRole(e.target.value)}
                          required
                        >
                          {roles.map((role) => (
                            <option key={role.role_id} value={role.role_id}>
                              {role.name}
                            </option>
                          ))}
                        </select>
                      </div> */}

                      <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                          id="password"
                          type="password"
                          className="form-control"
                          name="password"
                          value={password}
                          onChange={handleChange}
                          required
                        />
                      </div>

                      <div className="form-group my-2 d-grid">
                        <button
                          type="submit"
                          className="btn btn-primary"
                          disabled={loading ? true : false}
                        >
                          Register
                        </button>
                      </div>
                      <div className="mt-4 text-center">
                        Already have an account?{' '}
                        <Link className="text-info" to="/login">
                          Login
                        </Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Register;
