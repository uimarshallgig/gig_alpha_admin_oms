/* eslint-disable  no-unused-vars */
import React, { useState, useCallback } from 'react';
import {
  GoogleMap,
  useJsApiLoader,
  useLoadScript,
  Marker,
  InfoWindow,
} from '@react-google-maps/api';

import usePlacesAutocomplete, { getGeocode, getLatLng } from 'use-places-autocomplete';

import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
  ComboboxOptionText,
} from '@reach/combobox';
import '@reach/combobox/styles.css';

const containerStyle = {
  width: '100vw',
  height: '100vh',
};

const center = {
  lat: 9.082,
  lng: 8.6753,
};

const libraries = ['places'];
const options = { disableDefaultUI: true, zoomControl: true };

const MyGoogleMaps = () => {
  const [markers, setMarkers] = useState([]);
  const [selected, setSelected] = useState(null);
  const { isLoaded, loadError } = useLoadScript({
    id: 'google-map-script',
    libraries,
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
  });

  // useCallback help us to create a function that retains the same value,
  // And only re-renders if the value in the dependency array changes
  // useState causes re-renders,useCallback don't re-renders
  const onMapClick = useCallback((e) => {
    setMarkers((current) => [
      ...current,
      {
        lat: e.latLng.lat(),
        lng: e.latLng.lng(),
        time: new Date(),
      },
    ]);
  }, []);

  // Only load map on the page where it is needed and not each time the app loads.

  const mapRef = React.useRef();
  const onMapLoad = () =>
    useCallback((map) => {
      mapRef.current = map;
    }, []);

  if (loadError) return 'Error loading maps...';
  if (!isLoaded) return 'Loading maps...';

  // const onLoad = React.useCallback(function callback(map) {
  //   const bounds = new window.google.maps.LatLngBounds();
  //   map.fitBounds(bounds);
  //   setMap(map);
  // }, []);

  // const onUnmount = React.useCallback(function callback(map) {
  //   setMap(null);
  // }, []);
  return isLoaded ? (
    <>
      <div>
        <h2>Dear mapper</h2>
      </div>
      <Search />
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={10}
        options={options}
        onClick={onMapClick}
        onLoad={onMapLoad}
        // onUnmount={onUnmount}
      >
        {/* Child components, such as markers, info windows, etc. */}
        {markers.map((marker) => (
          <Marker
            key={marker.time.toISOString()}
            position={{ lat: marker.lat, lng: marker.lng }}
            onClick={() => {
              setSelected(marker);
            }}
          />
        ))}

        {selected ? (
          <InfoWindow
            position={{ lat: selected.lat, lng: selected.lng }}
            onCloseClick={() => setSelected(null)}
          >
            <div>
              <h2>Dear mapper</h2>
            </div>
          </InfoWindow>
        ) : null}
      </GoogleMap>
    </>
  ) : (
    <></>
  );
};

function Search() {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestion,
  } = usePlacesAutocomplete({
    requestOptions: {
      location: { lat: () => 42, lng: () => -45 },
      radius: 200 * 1000,
    },
  });
  return (
    <div>
      <Combobox onSelect={(address) => console.log(address)}>
        <ComboboxInput
          aria-labelledby="demo"
          value={value}
          onChange={(e) => setValue(e.target.value)}
          disabled={!ready}
          placeholder="Enter an address"
        />
        <ComboboxPopover>
          <ComboboxList aria-labelledby="demo">
            <ComboboxOption value="Apple" />
            <ComboboxOption value="Banana" />
            <ComboboxOption value="Orange" />
            <ComboboxOption value="Pineapple" />
            <ComboboxOption value="Kiwi" />
          </ComboboxList>
        </ComboboxPopover>
      </Combobox>
    </div>
  );
}

export default React.memo(MyGoogleMaps);
