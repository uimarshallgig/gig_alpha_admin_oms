import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const GoogPlace = ({
  name,
  type,
  placeholder,
  value,
  initPlaceAPI,
  placeInputRef,
  onChange,
}) => {
  // const placeInputRef = useRef(null);

  // const [place, setPlace] = useState(null);

  // initialize the google place autocomplete

  // const initPlaceAPI = () => {
  //   const autocomplete = new window.google.maps.places.Autocomplete(
  //     placeInputRef.current
  //   );

  //   window.google.maps.event.addListener(autocomplete, 'place_changed', function () {
  //     const places = autocomplete.getPlace();
  //     setPlace({
  //       address: places.formatted_address,
  //       lat: places.geometry.location.lat(),
  //       lng: places.geometry.location.lng(),
  //       localGovt: places.address_components,
  //     });
  //   });
  // };

  useEffect(() => {
    // const abortController = new AbortController();
    // const { signal } = abortController;
    // Access the input element in the DOM using 'placeInputRef.current'
    // Focus the input element for it to blink on page load
    placeInputRef.current.focus();
    initPlaceAPI();
    // return () => {
    //   setPlace({});
    //   abortController.abort();
    // };
    // localStorage.setItem('googlePlacesData', JSON.stringify(place));
  }, []);
  // const googlePlaces = place && place;
  // console.log('data from GoogPlace file', googlePlaces);
  // localStorage.setItem('googlePlaces', JSON.stringify(googlePlaces));

  return (
    <>
      <input
        ref={placeInputRef}
        className="form-control"
        id="delivery_address"
        type={type}
        placeholder={placeholder}
        name={name}
        value={value && value}
        onChange={onChange}
      />

      {/* {place && (
        <div style={{ marginTop: 20, lineHeight: '25px' }}>
          <div style={{ marginBottom: 10 }}>
            <b>Selected Place</b>
          </div>
          <div>
            <b>Address:</b> {place.address}
          </div>
          <div>
            <b>Lat:</b> {place.lat}
          </div>
          <div>
            <b>Lng:</b> {place.lng}
          </div>
          <div>
            <b>LocalGovt:</b>{' '}
            {place.localGovt.map((item) => {
              return (
                <div key={item.short_name}>
                  <p>{item.long_name}</p>
                </div>
              );
            })}
          </div>
        </div>
      )}
      {place && localStorage.setItem('googlePlacesData', JSON.stringify(place))} */}
    </>
  );
};

GoogPlace.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  initPlaceAPI: PropTypes.func.isRequired,
  placeInputRef: PropTypes.func.isRequired,
  placeholder: PropTypes.string,

  onChange: PropTypes.func.isRequired,
};
GoogPlace.defaultProps = {
  placeholder: 'Type here',
};

export default GoogPlace;
