import React from 'react';
import PropTypes from 'prop-types';
import { MdViewList } from 'react-icons/md';

const MetricsBoard = ({ title, figure, arrow }) => {
  const isTextRed = true;
  return (
    <>
      <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
        <div className="card">
          <div className="card-body">
            <h6 className="text-muted text-uppercase">{title}</h6>
            <div className="metric-value">
              <span className="fs-3 me-1 alpha-green">{figure}</span>
              <span className={!isTextRed ? 'alpha-red' : 'alpha-green'}>{arrow}</span>
            </div>
            <div className="metric-label d-inline-block float-right text-muted font-weight-bold fs-6">
              <span className="btn btn-sm btn-secondary py-0">
                <MdViewList className="fs-6 me-2" />
                View full report
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

MetricsBoard.propTypes = {
  title: PropTypes.string.isRequired,
  figure: PropTypes.number.isRequired,
  arrow: PropTypes.node.isRequired,
};

export default MetricsBoard;
