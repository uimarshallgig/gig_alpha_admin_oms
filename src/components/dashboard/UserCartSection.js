import React from 'react';
import { MdViewList } from 'react-icons/md';
import PropTypes from 'prop-types';

const UserCartSection = ({ title, figure, arrow }) => {
  const isTextRed = true;
  return (
    <>
      <form className="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12" id="dashboard-form">
        <h5 className="text-uppercase">user cart</h5>
        <div className="row g-3">
          <div className="col-sm-6">
            <input
              type="text"
              className="form-control"
              placeholder="Input user ID"
              aria-label="User Cart"
            />
          </div>
          <div className="d-grid gap-2 col-lg-3 col-12 col-sm">
            <button type="submit" className="btn btn-dark btn-sm text-uppercase">
              View cart
            </button>
          </div>
          <div className="d-grid gap-2 col-lg-3 col-12 col-sm">
            <button
              type="submit"
              className="btn border-btn btn-outline-dark btn-sm text-uppercase"
            >
              View store
            </button>
          </div>
        </div>
      </form>
      <div className="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
        <div className="card">
          <div className="card-body">
            <div>
              <span className="text-muted text-uppercase me-2">{title}</span>
              <div className="btn-group">
                <button
                  className="btn btn-outline-secondary btn-sm dropdown-toggle px-4 py-0"
                  type="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Today
                </button>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="!#">
                      Action
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="!#">
                      Another action
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="!#">
                      Something else here
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a className="dropdown-item" href="!#">
                      Separated link
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="metric-value">
              <span className="fs-3 me-1 alpha-red">{figure}</span>
              <span className={isTextRed ? 'alpha-red' : 'alpha-green'}>{arrow}</span>
            </div>
            <div className="metric-label d-inline-block float-right text-muted font-weight-bold fs-6">
              <span className="btn btn-sm btn-secondary py-0">
                <MdViewList className="fs-6 me-2" />
                View full report
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

UserCartSection.propTypes = {
  title: PropTypes.string.isRequired,
  figure: PropTypes.number.isRequired,
  arrow: PropTypes.node.isRequired,
};

export default UserCartSection;
