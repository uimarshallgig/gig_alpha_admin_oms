import React from 'react';
import { Link } from 'react-router-dom';
import {
  MdSearch,
  MdEmail,
  MdNotificationsNone,
  MdKeyboardArrowDown,
} from 'react-icons/md';
import { FaUserCircle } from 'react-icons/fa';
import '../assets/css/TopNav.css';
import '../assets/css/Dropdown.css';

const TopNav = () => {
  return (
    <>
      <div className="topbarContainer fixed-top">
        <div className="topbarLeft">
          <Link to="/" style={{ textDecoration: 'none' }}>
            <span className="logo">
              <img
                src="/assets/images/gig_logo_3.png"
                width="34"
                height="34"
                className="d-inline-block align-top"
                alt="gig logo"
              />
            </span>
          </Link>
        </div>
        <div className="topbarCenter">
          <div className="searchbar">
            <MdSearch className="searchIcon" />

            <input placeholder="Search for an item..." className="searchInput" />
          </div>
        </div>
        <div className="topbarRight">
          <div className="topbarLinks">
            <span className="topbarLink">Homepage</span>
            {/* <span className="topbarLink">Timeline</span> */}
          </div>
          <div className="topbarIcons ms-5">
            <div className="topbarIconItem">
              <MdEmail />
              <span className="topbarIconBadge">2</span>
            </div>
            <div className="topbarIconItem">
              <MdNotificationsNone />
              <span className="topbarIconBadge">1</span>
            </div>
            <div className="topbarIconItem dropdown">
              <FaUserCircle />
              {/* <span className="topbarIconBadge">1</span> */}
              <span className="navbarName py-auto ms-1 me-2">SuperAdmin cliff !</span>
              <MdKeyboardArrowDown />
              <div className="dropdown-content">
                <a href="!#">Login</a>
                <a href="!#">Profile</a>
                <a href="!#">Dashboard</a>
              </div>
            </div>
          </div>
          <Link to="/">
            {/* <img
              src={
                user.profilePicture
                  ? PF + user.profilePicture
                  : `${PF}person/noAvatar.png`
              }
              alt="avatar"
              className="topbarImg"
            /> */}
          </Link>
          {/* <Link to={`/profile/${user.username}`}>
            <img
              src={
                user.profilePicture
                  ? PF + user.profilePicture
                  : `${PF}person/noAvatar.png`
              }
              alt="avatar"
              className="topbarImg"
            />
          </Link> */}
        </div>
      </div>
    </>
  );
};

export default TopNav;
