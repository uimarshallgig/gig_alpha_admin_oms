/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
/* eslint-disable consistent-return */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import { MdArrowUpward, MdArrowDownward } from 'react-icons/md';
import UserActivityCard from '../cards/UserActivityCard';
import DashboardHeader from './DashboardHeader';
import MetricsBoard from './MetricsBoard';
import BasicTable from '../tables/BasicTable';
import MostSearchCat from '../cards/MostSearchCat';
import Sidebar from '../layout/Sidebar';
import './Dashboard.css';
import UserCartSection from './UserCartSection';
import { getAllRegularUsers } from '../../actions/userActions';

const Dashboard = () => {
  const dispatch = useDispatch();
  const alert = useAlert();
  // Map redux state to props of Home component
  const { loading, users, error } = useSelector((state) => state.regularUser);
  console.log(users && users.results);

  useEffect(() => {
    if (error) {
      // alert.success('success');
      return alert.error(error);
    }
    dispatch(getAllRegularUsers());
  }, [dispatch, alert, error]);
  // const pathId = window.location.pathname;
  // console.log(pathId, ' the pathname is home');

  return (
    <div className="dashboard-main-wrapper">
      <Sidebar />
      <div className="dashboard-wrapper">
        <div className="dashboard-ecommerce">
          <div className="container-fluid dashboard-content ">
            <DashboardHeader />
            <div className="ecommerce-widget">
              <div className="row shadow-lg pt-4">
                {users.results &&
                  users.results.map((merchant) => (
                    <MetricsBoard
                      key={merchant._id}
                      title="basic merchant"
                      figure={Number(merchant.user_type === 'basic')}
                      arrow={<MdArrowUpward className="fs-3 pb-2" />}
                    />
                  ))}

                <MetricsBoard
                  title="class merchant"
                  figure={Number(55678)}
                  arrow={<MdArrowDownward className="fs-3 pb-2 alpha-red" />}
                />
                <MetricsBoard
                  title="new orders"
                  figure={Number(55678)}
                  arrow={<MdArrowDownward className="fs-3 pb-2 alpha-red" />}
                />
                <MetricsBoard
                  title="total transactions"
                  figure={Number(55678)}
                  arrow={<MdArrowUpward className="fs-3 pb-2" />}
                />
                <hr className="mx-auto w-90" />
                <div className="row">
                  <UserCartSection
                    title="payment"
                    figure={Number(55678)}
                    arrow={<MdArrowDownward className="fs-3 pb-2 alpha-red" />}
                  />
                </div>
              </div>
              <div className="row">
                {/* <BasicTable />
                <UserActivityCard />
                <MostSearchCat /> */}
              </div>
              {/* <div className="row">
                <BasicTable />
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
