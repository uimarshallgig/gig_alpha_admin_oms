import React from 'react';
import { MdDashboard } from 'react-icons/md';

const DashboardHeader = () => {
  return (
    <>
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div className="page-header">
            <h2 className="pageheader-title">
              <MdDashboard className="me-2" />
              {}
              <span>Main Dashboard</span>
            </h2>
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardHeader;
