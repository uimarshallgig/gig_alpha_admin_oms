import React from 'react';
import { Line } from 'react-chartjs-2';

const SalesPerformanceChart = () => {
  // The data we want to visualize is passed as props to the Line chart.
  return (
    <>
      <Line data={1} />
    </>
  );
};

export default SalesPerformanceChart;
