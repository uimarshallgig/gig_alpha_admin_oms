import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
//  Helmet is used for SEO

const MetaData = ({ title }) => {
  return (
    <Helmet>
      <title>{`${title} - GIG-ALPHA`}</title>
    </Helmet>
  );
};

MetaData.propTypes = {
  title: PropTypes.string.isRequired,
};

export default MetaData;
