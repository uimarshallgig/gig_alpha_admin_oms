/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { CirclePicker } from 'react-color';
// import { SwatchesPicker } from 'react-color';
import PropTypes from 'prop-types';

const ColorPicker = ({ color, handleChangeComplete }) => {
  console.log(color);

  const colorItems = color ? color.split(',') : [];
  console.log(colorItems);

  const [colors, setColors] = useState(colorItems);

  return (
    <>
      <CirclePicker colors={colors} onChangeComplete={handleChangeComplete} />
    </>
  );
};

ColorPicker.propTypes = {
  color: PropTypes.string.isRequired,
};

export default ColorPicker;
