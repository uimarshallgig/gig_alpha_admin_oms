import PropTypes from 'prop-types';
import './modal.css';

const Modal = ({ handleClose, show, children }) => {
  const showHideClassName = show ? 'modal display-block' : 'modal display-none';

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        {children}
        <button
          type="button"
          onClick={handleClose}
          className="close border border-0 mt-2"
        >
          X
        </button>
      </section>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
  show: PropTypes.objectOf(Boolean).isRequired,
};

export default Modal;
