/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */

import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RaveProvider, RavePaymentButton } from 'react-ravepayment';
import { initializeTransaction } from '../../actions/orderActions';
// import numbro from 'numbro';

// const totalAmount = numbro(2000).format({ thousandSeparated: true });
const config = {
  txref: new Date().toString(),
  customer_email: 'akpan.marshall@giglogistics.ng',
  customer_phone: '08064415336',
  amount: 500,
  PBFPubKey: process.env.REACT_APP_FLWPUBK_KEY,
  production: true,
  onSuccess: () => {},
  onClose: () => {},
  currency: 'NGN',
  payment_options: 'card,mobilemoney,ussd',

  customizations: {
    title: 'my Payment Title',
    description: 'Payment for items in cart',
    logo: 'https://st2.depositphotos.com/4403291/7418/v/450/depositphotos_74189661-stock-illustration-online-shop-log.jpg',
  },
};

const Rave = () => {
  const [paymentInfo, setPaymentInfo] = useState({
    amount: config.amount,
    payment_gateway: 'flutterwave',
  });

  const { amount, payment_gateway } = paymentInfo;

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.set('amount', amount);
    formData.set('payment_gateway', payment_gateway);

    dispatch(initializeTransaction(formData));
  };

  const handleChange = (e) => {
    setPaymentInfo({ ...paymentInfo, [e.target.name]: e.target.value });
  };
  return (
    <form onSubmit={handleSubmit}>
      <RaveProvider {...config}>
        <div className="d-grid gap-2 btn-success-1 mb-3 rounded">
          <RavePaymentButton
            type="button"
            className="btn text-capitalize fs-6 pt-2 pb-0"
            id="submit_button"
          >
            Proceed to payment
          </RavePaymentButton>
        </div>
      </RaveProvider>
    </form>
  );
};

export default Rave;
