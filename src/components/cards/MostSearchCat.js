import React from 'react';

const MostSearchCat = () => {
  return (
    <>
      <div className="col-xl-3 col-lg-12 col-md-6 col-sm-12 col-12">
        <div className="card">
          <h5 className="card-header">Most Search Category</h5>
          <div className="card-body p-0">
            <ul className="country-sales list-group list-group-flush">
              <li className="country-sales-content list-group-item">
                <span className="mr-2">
                  <i className="flag-icon flag-icon-us" title="us" id="us" />{' '}
                </span>
                <span className="">fashion</span>
              </li>
              <li className="list-group-item country-sales-content">
                <span className="mr-2">
                  <i className="flag-icon flag-icon-ca" title="ca" id="ca" />
                </span>
                <span className="">Electronics</span>
              </li>
              <li className="list-group-item country-sales-content">
                <span className="mr-2">
                  <i className="flag-icon flag-icon-ru" title="ru" id="ru" />
                </span>
                <span className="">Grocery</span>
              </li>
              <li className="list-group-item country-sales-content">
                <span className=" mr-2">
                  <i className="flag-icon flag-icon-in" title="in" id="in" />
                </span>
                <span className="">Health and beauty</span>
              </li>
              <li className="list-group-item country-sales-content">
                <span className=" mr-2">
                  <i className="flag-icon flag-icon-fr" title="fr" id="fr" />
                </span>
                <span className="">Home appliances</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default MostSearchCat;
