/* eslint-disable  react/self-closing-comp */
import React from 'react';

const UserActivityCard = () => {
  return (
    <>
      <div className="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12">
        <div className="card">
          <h5 className="card-header"> Top Selling Products</h5>
          <div className="card-body p-0">
            <ul className="social-sales list-group list-group-flush">
              <li className="list-group-item social-sales-content d-flex justify-content-between align-items-center">
                <div className="social-sales-icon-circle facebook-bgcolor mr-2">
                  {/* <i className="fab fa-facebook-f" /> */}
                  <img
                    src="assets/images/avatar-1.jpg"
                    alt=""
                    className="user-avatar-md rounded-circle"
                  />
                </div>
                <span className="social-sales-name d-inline-block">Name</span>
                <span className="social-sales-name d-inline-block">
                  <div className="reviews">
                    <ul className="stars">
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star-o"></i>
                      </li>
                    </ul>
                  </div>
                </span>
                <span className="social-sales-count text-dark d-inline-block">
                  120 Sales
                </span>
              </li>
              <li className="list-group-item social-sales-content d-flex justify-content-between align-items-center">
                <div className="social-sales-icon-circle facebook-bgcolor mr-2">
                  {/* <i className="fab fa-facebook-f" /> */}
                  <img
                    src="assets/images/avatar-1.jpg"
                    alt=""
                    className="user-avatar-md rounded-circle"
                  />
                </div>
                <span className="social-sales-name d-inline-block">Name</span>
                <span className="social-sales-name d-inline-block">
                  <div className="reviews">
                    <ul className="stars">
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star-o"></i>
                      </li>
                    </ul>
                  </div>
                </span>
                <span className="social-sales-count text-dark d-inline-block">
                  120 Sales
                </span>
              </li>
              <li className="list-group-item social-sales-content d-flex justify-content-between align-items-center">
                <div className="social-sales-icon-circle facebook-bgcolor mr-2">
                  {/* <i className="fab fa-facebook-f" /> */}
                  <img
                    src="assets/images/avatar-1.jpg"
                    alt=""
                    className="user-avatar-md rounded-circle"
                  />
                </div>
                <span className="social-sales-name d-inline-block">Name</span>
                <span className="social-sales-name d-inline-block">
                  <div className="reviews">
                    <ul className="stars">
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star-o"></i>
                      </li>
                    </ul>
                  </div>
                </span>
                <span className="social-sales-count text-dark d-inline-block">
                  120 Sales
                </span>
              </li>
              <li className="list-group-item social-sales-content d-flex justify-content-between align-items-center">
                <div className="social-sales-icon-circle facebook-bgcolor mr-2">
                  {/* <i className="fab fa-facebook-f" /> */}
                  <img
                    src="assets/images/avatar-1.jpg"
                    alt=""
                    className="user-avatar-md rounded-circle"
                  />
                </div>
                <span className="social-sales-name d-inline-block">Name</span>
                <span className="social-sales-name d-inline-block">
                  <div className="reviews">
                    <ul className="stars">
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star"></i>
                      </li>
                      <li className="list-unstyled">
                        <i className="fa fa-star-o"></i>
                      </li>
                    </ul>
                  </div>
                </span>
                <span className="social-sales-count text-dark d-inline-block">
                  120 Sales
                </span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserActivityCard;
