/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable  no-unused-vars */
/* eslint-disable  no-shadow */
/* eslint-disable  consistent-return */
/* eslint-disable  react/jsx-props-no-spreading */
/* eslint-disable  no-underscore-dangle */
/* eslint-disable  array-callback-return */
import React, { useState, useEffect, useCallback } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import axios from 'axios';

import { useHistory } from 'react-router-dom';
import { PaystackButton } from 'react-paystack';
import { MdClear, MdKeyboardBackspace } from 'react-icons/md';
import { FaQuestionCircle } from 'react-icons/fa';
import './PaymentDetails.css';
import Rave from '../payments/Rave';
import {
  getPaymentMethod,
  initializeTransaction,
  verifyTransaction,
} from '../../actions/orderActions';
import Loader from '../shared/Loader';
import PayStack from '../payments/PayStack';

const PaymentDetails = () => {
  const alert = useAlert();
  const history = useHistory();
  const dispatch = useDispatch();

  const [paymentChoice, setPaymentChoice] = useState('paystack');

  // Map redux state to props of Home component
  const { loading, paymentMethods, error } = useSelector((state) => state.paymentMethod);

  // const paymentTypes = paymentMethods && paymentMethods;
  // console.log(paymentTypes);
  const paymentTypes = paymentMethods && paymentMethods;
  console.log(paymentTypes);
  console.log(paymentTypes?.results);
  const paymentGateway =
    paymentTypes && paymentTypes?.results?.find((i) => i.name === paymentChoice);
  console.log(paymentGateway);

  const {
    loading: transactLoader,
    transactionInfo,
    error: transactError,
  } = useSelector((state) => state.initializeTransaction);
  console.log(transactionInfo);

  const {
    loading: transactVerifyLoader,
    transactVerificationInfo,
    error: transactVerifyError,
  } = useSelector((state) => state.verifyTransaction);

  console.log(transactVerificationInfo);
  // Delivery fee
  // const deliveryFee = localStorage.getItem('shipmentPriceData')
  //   ? JSON.parse(localStorage.getItem('shipmentPriceData'))
  //   : null;
  // console.log(deliveryFee.Object.GrandTotal);

  const { loading: shipmentLoader, shipmentPriceDetails } = useSelector(
    (state) => state.shippingPrice
  );
  console.log(shipmentPriceDetails);

  const shipmentPriceData = shipmentPriceDetails && shipmentPriceDetails?.Object;

  localStorage.setItem('shipmentPriceData', JSON.stringify(shipmentPriceDetails));

  console.log('shipment data for deliveryFee from paymentDetails: ', shipmentPriceData);

  // Product price
  const productPrice = localStorage.getItem('productPrice')
    ? JSON.parse(localStorage.getItem('productPrice'))
    : '';
  const orderQty = localStorage.getItem('orderQty')
    ? JSON.parse(localStorage.getItem('orderQty'))
    : [];
  console.log('Price and Qty from PaymentDetails: ', productPrice, orderQty);

  const productTotalPrice = Number(productPrice) * Number(orderQty);
  console.log('total price', productTotalPrice);
  // const shipmentFee = Number(
  //   shipmentPriceDetails && shipmentPriceDetails?.Object?.GrandTotal
  // );
  const shipmentFee = shipmentPriceDetails?.Object?.GrandTotal;

  console.log(shipmentFee);

  const productGrandTotal = productTotalPrice + shipmentFee;
  // Number(shipmentPriceDetails && shipmentPriceDetails?.Object?.GrandTotal);
  console.log(typeof productGrandTotal);

  // ***************Paystack*******************************
  const publicKey = process.env.REACT_APP_PAYSTACK_PUBLIC_KEY;
  // const amount = Number(productGrandTotal) * 100;
  // const payStackAmt = Number(productGrandTotal) * 100;
  const payStackAmt = Number(productTotalPrice) * 100;
  const amount = payStackAmt;
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [data, setData] = useState(null);
  console.log(data);
  console.log(data.dataFetched.data.results);

  const paymentGateway1 =
    data && data.dataFetched.data.results.find((i) => i.name === paymentChoice);
  console.log(paymentGateway1);

  const transactData = {
    // amount: productGrandTotal,
    amount: productTotalPrice,
    // payment_gateway: '614ad15ffbf1951d2cc803ce',
    // payment_gateway: paymentGateway && paymentGateway?._id,
    payment_gateway: paymentGateway1?._id,
  };

  console.log(transactData?.paymentGateway?._id);
  console.log(transactData);
  // const fetchData = async () => {
  //   const paymentMtd = await dispatch(getPaymentMethod());
  //   const initTransactData = await dispatch(initializeTransaction(transactData));

  //   setData({
  //     paymentMtd,
  //     initTransactData,
  //   });
  // };

  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const getData = async () => {
    const dataFetched = await axios.get(
      'https://alpha-order-sandbox-api.com/api/v1/gateway/list'
    );
    const initTransactData = await axios.post(
      'https://alpha-order-sandbox-api.com/api/v1/transaction/initiate',
      transactData,
      config
    );
    setData({
      dataFetched,
      initTransactData,
    });
  };

  useEffect(() => {
    if (error) {
      return alert.error(error);
    }
    // dispatch(getPaymentMethod());
    // if (transactData.paymentGateway?._id !== null) {
    //   dispatch(initializeTransaction(transactData));
    // }
    // if (transactData.paymentGateway?._id !== undefined) {
    //   dispatch(initializeTransaction(transactData));
    // }

    getData();
  }, [dispatch, alert, error]);

  // ProductInfo in a station
  const stationItems = localStorage.getItem('stationItems')
    ? JSON.parse(localStorage.getItem('stationItems'))
    : [];
  // Shipment Data

  const shipmentData = localStorage.getItem('shipmentData')
    ? JSON.parse(localStorage.getItem('shipmentData'))
    : [];
  console.log('shipmentData from PaymentDetails: ', shipmentData);
  console.log(
    'customerCode from PaymentDetails: ',
    shipmentData.Merchants[0].CustomerCode
  );

  const receiverStationIdData = localStorage.getItem('receiverStationId')
    ? JSON.parse(localStorage.getItem('receiverStationId'))
    : '';
  console.log('receiverStationId', receiverStationIdData);

  console.log(typeof receiverStationIdData);

  const transactVerificationData = {
    trxnRef: transactionInfo && transactionInfo?.trxnRef,
    id: paymentGateway && paymentGateway?._id,
    // totalCost: Number(productGrandTotal), // directly from total cost in LocalStorage
    totalCost: Number(productTotalPrice), // directly from total cost in LocalStorage
    ReceiverAddress: shipmentData.ReceiverAddress,
    CustomerCode: shipmentData.Merchants[0].CustomerCode,
    SenderLocality: shipmentData.Merchants[0].SenderLocality,
    SenderAddress: shipmentData.Merchants[0].SenderAddress,
    ReceiverPhoneNumber: shipmentData.ReceiverPhoneNumber,
    VehicleType: shipmentData.VehicleType,
    SenderPhoneNumber: shipmentData.Merchants[0].SenderPhoneNumber,
    SenderName: shipmentData.Merchants[0].SenderName,
    ReceiverName: shipmentData.ReceiverName,
    UserId: stationItems.userId,
    ReceiverStationId: receiverStationIdData,
    SenderStationId: shipmentData.SenderStationId,
    ReceiverLocation: shipmentData.ReceiverLocation,
    SenderLocation: shipmentData.Merchants[0].SenderLocation,

    PreShipmentItems: shipmentData.Merchants[0].PreShipmentItems,
  };

  const componentProps = {
    email,
    amount,
    metadata: {
      name,
      phone,
    },
    publicKey,
    // reference: new Date().getTime().toString(),
    reference: transactionInfo?.trxnRef,
    // text: 'Proceed to Payment',
    text: 'Pay Now',
    onSuccess: () => {
      setEmail('');
      setName('');
      setPhone('');
      dispatch(verifyTransaction(transactVerificationData));
      if (transactVerifyError?.status === 400) {
        history.push('/failure');
      } else {
        history.push('/success');
      }
    },
    onClose: () => alert.success("Wait! You need this oil, don't go!!!!"),
  };

  // ***************************************************************

  const goBack = () => {
    history.goBack();
  };

  // const closeBtn = () => {
  //   const clear = document.querySelector('.close-btn');

  //   clear.addEventListener('click', function () {
  //     const mainContent = document.querySelector('.main-content');
  //     mainContent.style.visibility = 'hidden';
  //   });
  // };

  const showBtn1 = () => {
    document.getElementById('hide_div').style.display = 'none';
    document.getElementById('paystack-section').style.display = 'block';
  };
  const showBtn2 = () => {
    document.getElementById('hide_div').style.display = 'block';
    document.getElementById('paystack-section').style.display = 'none';
  };

  return (
    <>
      {loading && transactVerifyLoader && transactLoader ? (
        <Loader />
      ) : (
        <div className="container-fluid py-3 payment-flow-card bg-light">
          {/* {paymentChoice} */}
          <div className="row">
            <div className="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
              <div id="pay-invoice" className="card">
                <div className="card-body">
                  <div className="card-title">
                    {/* <h3 className="text-center">Payment</h3> */}
                    <div className="position-relative">
                      <span className="text-center fw-bold fs-6 text-uppercase d-block payment-flow-header">
                        Payment
                      </span>
                      <span className="position-absolute top-0 end-0 close-btn">
                        <MdClear onClick={goBack} />
                      </span>
                      <span className="position-absolute top-0 start-0 close-btn">
                        <MdKeyboardBackspace onClick={goBack} />
                      </span>
                    </div>
                  </div>
                  <hr />
                  <h6 className="d-flex justify-content-start mb-2 payment-flow-sub-header">
                    Choose payment method
                  </h6>
                  <div className="payment-logo mb-2 d-flex justify-content-start">
                    {/* {paymentMethods &&
                      paymentMethods.results.map((item) => {
                        <div className="form-check me-4" key={item._id}>
                          <input
                            className="form-check-input my-2"
                            type="radio"
                            name={item.name}
                            id="paystack"
                            checked={
                              (paymentChoice === paymentChoice.name) === 'paystack'
                            }
                            value={item._id}
                            onChange={(e) =>
                              setPaymentChoice(...paymentChoice, {
                                name: e.target.name,
                                slug: e.target.value,
                              })
                            }
                            onClick={() => showBtn1()}
                          />
                          <label className="form-check-label" htmlFor="paystack">
                            <img src="assets/images/paystack_logo.svg" alt="paystack" />
                          </label>
                        </div>;
                      })} */}
                    <div className="form-check me-4">
                      <input
                        className="form-check-input my-2"
                        type="radio"
                        name="paystack"
                        id="paystack"
                        checked={paymentChoice === 'paystack'}
                        value="paystack"
                        onChange={(e) => setPaymentChoice(e.target.value)}
                        onClick={() => showBtn1()}
                      />
                      <label className="form-check-label" htmlFor="paystack">
                        <img src="assets/images/paystack_logo.svg" alt="paystack" />
                      </label>
                    </div>

                    <div className="form-check">
                      <input
                        className="form-check-input my-2"
                        type="radio"
                        name="flutterwave"
                        id="flutterwave"
                        checked={paymentChoice === 'flutterwave'}
                        value="flutterwave"
                        onChange={(e) => setPaymentChoice(e.target.value)}
                        onClick={() => showBtn2()}
                      />
                      <label className="form-check-label" htmlFor="flutterwave">
                        <img src="assets/images/flutterwave_logo.svg" alt="flutterwave" />
                      </label>
                    </div>
                  </div>
                  <hr />

                  <h6 className="d-flex justify-content-start mb-2 payment-flow-sub-header">
                    Enter Card details
                  </h6>
                  <div className="cards-logo mb-2">
                    <img
                      src="assets/images/visa.png"
                      alt="visa"
                      className="d-inline-block"
                    />
                    <img
                      src="assets/images/mastercard.png"
                      alt="verve"
                      className="d-inline-block ms-2 me-2"
                    />
                    <img
                      src="assets/images/verve.png"
                      alt="verve"
                      className="d-inline-block"
                    />
                  </div>
                  <section className="paystack-section" id="paystack-section">
                    <form className="needs-validation">
                      {/* <div className="form-group payment-flow-sub-header mb-2">
                      <label htmlFor="cc-number" className="control-label mb-1">
                        Card number
                      </label>
                      <input
                        id="cc-number"
                        name="cc-number"
                        type="tel"
                        className="form-control cc-number identified visa"
                        required
                        placeholder="0000 0000 0000 0000"
                      />
                      <span className="invalid-feedback">
                        Enter a valid 12 to 16 digit card number
                      </span>
                    </div> */}

                      <div className="checkout form-group payment-flow-sub-header mb-2">
                        <label className="control-label mb-1">Name</label>
                        <input
                          className="form-control cc-number identified"
                          type="text"
                          id="name"
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                          required
                          placeholder="Your Name"
                        />
                      </div>
                      <div className="checkout-field form-group payment-flow-sub-header mb-2">
                        <label className="control-label mb-1">Email</label>
                        <input
                          className="form-control cc-number identified"
                          type="text"
                          id="email"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                          placeholder="Your Email"
                          required
                        />
                      </div>
                      <div className="checkout-field form-group payment-flow-sub-header mb-2">
                        <label className="control-label mb-1">Phone</label>
                        <input
                          className="form-control cc-number identified"
                          type="text"
                          id="phone"
                          value={phone}
                          onChange={(e) => setPhone(e.target.value)}
                          placeholder="Your Phone Number"
                        />
                      </div>

                      {/* <div className="row mb-2">
                      <div className="col-6">
                        <div className="form-group">
                          <label
                            htmlFor="cc-exp"
                            className="control-label mb-1 payment-flow-sub-header"
                          >
                            Expiration
                          </label>
                          <input
                            id="cc-exp"
                            name="cc-exp"
                            type="tel"
                            className="form-control cc-exp"
                            required
                            placeholder="MM / YY"
                            autoComplete="cc-exp"
                          />
                          <span className="invalid-feedback">
                            Enter the expiration date
                          </span>
                        </div>
                      </div>
                      <div className="col-6">
                        <label
                          htmlFor="x_card_code"
                          className="control-label mb-1  payment-flow-sub-header"
                        >
                          CVV
                        </label>
                        <span
                          title="CVV is a credit or debit card number, the last three digit number printed on the back of your card"
                          className="d-inline-block text-muted ms-2 tooltip-text"
                        >
                          <FaQuestionCircle />
                        </span>

                        <div className="input-group">
                          <input
                            id="x_card_code"
                            name="x_card_code"
                            type="tel"
                            className="form-control cc-cvc"
                            required
                            placeholder="000"
                          />
                        </div>
                      </div>
                    </div> */}
                      {/* Price/quantity paystack */}

                      <div className="d-flex justify-content-between price-quantity">
                        <p className="text-muted">V.A.T</p>
                        <div className="stockCounter d-inline">&#8358;0.00</div>
                      </div>
                      <div className="d-flex justify-content-between price-quantity">
                        <p className="text-muted payment-flow-sub-header">Delivery fee</p>
                        <div className="stockCounter d-inline">
                          &#8358;
                          {shipmentPriceDetails &&
                            shipmentPriceDetails?.Object?.GrandTotal}
                        </div>
                      </div>
                      <hr />
                      <div className="d-flex justify-content-between price-quantity">
                        <p className="payment-flow-sub-header">Total</p>
                        <div className="stockCounter d-inline">
                          {/* &#8358; {productGrandTotal && productGrandTotal} */}
                          &#8358; {productTotalPrice}
                        </div>
                      </div>
                    </form>
                    <div className="d-grid gap-2 btn-success-1 mb-3 rounded">
                      <PaystackButton
                        className="paystack-button btn text-capitalize fs-6"
                        id="submit_button"
                        {...componentProps}
                      />
                    </div>
                    {/* <PayStack /> */}
                  </section>

                  <section id="hide_div" className="hide">
                    <div className="needs-validation">
                      {/* Price/quantity flutterwave */}
                      <section>
                        <div className="d-flex justify-content-between price-quantity">
                          <p className="text-muted">V.A.T</p>
                          <div className="stockCounter d-inline">&#8358;43215</div>
                        </div>
                        <div className="d-flex justify-content-between price-quantity">
                          <p className="text-muted payment-flow-sub-header">
                            Delivery fee
                          </p>
                          <div className="stockCounter d-inline">&#8358;3215</div>
                        </div>
                        <hr />
                        <div className="d-flex justify-content-between price-quantity">
                          <p className="payment-flow-sub-header">Total</p>
                          <div className="stockCounter d-inline">&#8358;53215</div>
                        </div>

                        <div className="d-grid gap-2 btn-success-1 mb-3 rounded">
                          {/* <button
                        type="button"
                        className="btn text-capitalize fs-6"
                        id="submit_button"
                        onClick={submitHandler}
                      >
                        Proceed to Payment
                      </button> */}
                          <Rave />
                        </div>
                      </section>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default PaymentDetails;
