/* eslint-disable  jsx-a11y/label-has-associated-control */
/* eslint-disable  react/self-closing-comp */
/* eslint-disable  no-unused-vars */
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import MetaData from '../shared/MetaData';
import CheckOutSteps2 from './CheckOutSteps2';
import OrderSuccess from './OrderSuccess';
import Modal from '../shared/Modal';

const Payment = () => {
  const history = useHistory();
  const [show, showModal] = useState(false);

  const submitHandler = (e) => {
    e.preventDefault();
    history.push('/success');
  };

  return (
    <>
      <MetaData title="Payment" />

      <div className="row wrapper mt-5" id="shipping">
        <div className="col-10 col-lg-5">
          <div className="card bg-white shadow-lg mb-4">
            <h5 className="text-center pt-2 h3">Checkout</h5>
            <CheckOutSteps2 shipping confirmOrder payment />
            <hr className="w-75 mx-auto text-muted border border-2" />

            <h3 className="text-capitalize text-start ms-5 text-muted mb-0">
              Payment method
            </h3>
            <div className="card-body px-1">
              <form className="" onSubmit={submitHandler}>
                <div className="check-option-wrapper bg-light py-3 px-2">
                  <div className="form-check">
                    <input
                      className="form-check-input my-2"
                      type="radio"
                      name="paystack"
                      id="paystack"
                    />
                    <label className="form-check-label" htmlFor="paystack">
                      <img src="assets/images/paystack_logo.svg" alt="paystack" />
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input my-2"
                      type="radio"
                      name="flutterwave"
                      id="flutterwave"
                      checked
                    />
                    <label className="form-check-label" htmlFor="flutterwave">
                      <img src="assets/images/flutterwave_logo.svg" alt="flutterwave" />
                    </label>
                  </div>
                </div>

                {/* Price/quantity */}
                <div className="d-flex justify-content-between price-quantity">
                  <p>Amount</p>
                  <div className="stockCounter d-inline">N43215</div>
                </div>
                <div className="d-flex justify-content-between price-quantity">
                  <p>Delivery fee</p>
                  <div className="stockCounter d-inline">N3215</div>
                </div>
                <hr />
                <div className="d-flex justify-content-between price-quantity">
                  <p>Total amount</p>
                  <div className="stockCounter d-inline">N53215</div>
                </div>
                {/* Checkout btn */}
                <div className="d-grid gap-2">
                  <button
                    type="button"
                    className="btn btn-dark d-inline ms-4 rounded-pill text-uppercase fw-bold"
                    onClick={submitHandler}
                    // onClick={() => showModal(true)}
                  >
                    proceed
                  </button>
                </div>
                {/* <Modal show={show} handleClose={() => showModal(false)}>
                  <OrderSuccess />
                </Modal> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Payment;
