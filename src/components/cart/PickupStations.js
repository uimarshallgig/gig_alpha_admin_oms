/* eslint-disable  jsx-a11y/interactive-supports-focus */
/* eslint-disable  jsx-a11y/click-events-have-key-events */
/* eslint-disable  no-unused-vars */
/* eslint-disable  jsx-a11y/mouse-events-have-key-events */

import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { MdClear } from 'react-icons/md';

import { FaTruckPickup, FaBuilding } from 'react-icons/fa';
import MetaData from '../shared/MetaData';

import './PickupStation.css';
import { addItemsToGigLocStations, clearErrors } from '../../actions/orderActions';

const PickupStations = ({ productId }) => {
  const [location, setLocation] = useState({
    location: '',
    showBtn: false,
  });

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addItemsToGigLocStations(productId));
  }, [dispatch]);

  // const handleChange = (e) => {
  //   setLocation({ location: e.target.value, showBtn: true });
  // };

  const submitHandler = (e) => {
    e.preventDefault();
    if (location === 'Door delivery') {
      history.push('/delivery-details');
    } else if (location === 'GIG Pickup Station') {
      history.push('/pickup-stations');
    }
  };

  // const submitBtn = () => {
  //   const sbtBtn = document.querySelector('#submit_button[disabled]');
  //   if (sbtBtn.disabled === true) {
  //     sbtBtn.style.background = 'grey';
  //   } else {
  //     sbtBtn.style.background = 'red';
  //   }
  // };

  return (
    <>
      <MetaData title="Product Details" />

      <section className="main-content pickup-stations">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-sm-6 col-md-4 offset-sm-3 offset-md-4">
              <div className="info-card info-card--danger bg-white text-center mb-4 rounded-lg rounded-2">
                <div className="position-relative">
                  <span className="text-center fw-bold fs-6">Checkout</span>
                  {/* <span className="position-absolute top-0 end-0 close-btn">
                    <MdClear />
                  </span> */}
                </div>
                <hr />
                <div className="d-flex flex-column">
                  <h6 className="d-flex justify-content-start mb-4">
                    Select a delivery method
                  </h6>
                  <div className="door-delivery">
                    <div className="d-flex flex-row">
                      <div className="me-2">
                        <FaTruckPickup className="pickup-truck text-success-1" />
                      </div>
                      <div className="d-flex flex-column align-self-center text-start">
                        <h6 className="fs-6">Door delivery</h6>
                        <p className="text-muted fs-6">
                          Delivered between Thursday 26 Aug and Friday 27 Aug
                        </p>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault1"
                          checked={location === 'Door delivery'}
                          value="Door delivery"
                          onChange={(e) => setLocation(e.target.value)}
                        />
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>

                <div className="door-delivery mb-5">
                  <div className="d-flex flex-row">
                    <div className="me-2">
                      <FaBuilding className="pickup-truck text-success-1" />
                    </div>
                    <div className="d-flex flex-column align-self-center text-start">
                      <h6 className="fs-6">GIG Pickup Station</h6>
                      <p className="text-muted fs-6">
                        Ready for pickup between Thursday 26 Aug and Friday 27 Aug
                      </p>
                    </div>

                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="flexRadioDefault"
                        id="flexRadioDefault1"
                        checked={location === 'GIG Pickup Station'}
                        value="GIG Pickup Station"
                        onChange={(e) => setLocation(e.target.value)}
                      />
                    </div>
                  </div>
                </div>

                <div className="d-grid gap-2 btn-success-1 mb-3">
                  <button
                    type="button"
                    className="btn text-capitalize fs-6"
                    id="submit_button"
                    disabled={
                      location !== 'Door delivery' && location !== 'GIG Pickup Station'
                    }
                    onClick={submitHandler}
                  >
                    Choose Pickup Station
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* <div className="row justify-content-center">
        <div className="col-6 mt-5 text-center">
          <img
            className="my-5 img-fluid d-block mx-auto"
            src="/assets/images/order_success.png"
            alt="Order Success"
            width="200"
            height="200"
          />

          <h2>Your Order has been placed successfully.</h2>

          <Link to="/orders/me">Go to Orders</Link>
        </div>
      </div> */}
    </>
  );
};

PickupStations.propTypes = {
  productId: PropTypes.string.isRequired,
};

export default PickupStations;
