/* eslint-disable  no-unused-vars */
/* eslint-disable  consistent-return */
/* eslint-disable   react/prop-types */
/* eslint-disable   jsx-a11y/label-has-associated-control */
/* eslint-disable   no-use-before-define */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import { MdClear, MdKeyboardBackspace } from 'react-icons/md';

import MetaData from '../shared/MetaData';
import './PickupStation.css';
import { getServiceCenters } from '../../actions/orderActions';

const SelectPickupLocation = ({ match }) => {
  const alert = useAlert();
  // Map redux state to props of Home component
  const { loading, serviceCenters, error } = useSelector((state) => state.serviceCenters);
  const history = useHistory();
  const dispatch = useDispatch();

  // List of GIG local stations
  // const stationInfo = localStorage.getItem('stationInfo')
  //   ? JSON.parse(localStorage.getItem('stationInfo'))
  //   : [];
  // console.log(stationInfo.results.Object);
  // if (error) {
  //   return alert.error(error);
  // }
  const { stationInfo } = useSelector((state) => state.station);
  console.log(stationInfo);
  // console.log(stationInfo.Object);
  console.log(stationInfo && stationInfo.Code);

  const stationList = stationInfo && stationInfo;
  console.log('stationLists', stationList);

  // useEffect(() => {
  //   dispatch(getServiceCenters(match.params.stationId));
  // }, [dispatch, alert, error, match.params.stationId]);

  const submitHandler = (e) => {
    e.preventDefault();

    history.push('/selected-location');
  };

  const [station, setStation] = useState(stationInfo.StationName);

  // const closeBtn = () => {
  //   const clear = document.querySelector('.close-btn');

  //   clear.addEventListener('click', function () {
  //     const mainContent = document.querySelector('.main-content');
  //     mainContent.style.visibility = 'hidden';
  //   });
  // };

  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <MetaData title="Selected Pickup Location" />

      <section className="main-content pickup-stations">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-sm-6 col-md-4 offset-sm-3 offset-md-4">
              <div className="info-card info-card--danger bg-white text-center mb-4 rounded-lg rounded-2">
                <div className="position-relative">
                  <span className="text-center fw-bold fs-6 text-uppercase">
                    select pickup location
                  </span>
                  <span className="position-absolute top-0 end-0 close-btn">
                    <MdClear onClick={goBack} />
                  </span>
                  <span className="position-absolute top-0 start-0 close-btn">
                    <MdKeyboardBackspace onClick={goBack} />
                  </span>
                </div>
                <hr />
                <div className="d-flex flex-column">
                  <form className="mb-4">
                    {/* <div className="form-group mb-3">
                      <label htmlFor="station_field">Stations</label>
                      <select
                        id="station_field"
                        className="form-control"
                        value={station}
                        onChange={(e) => setStation(e.target.value)}
                        required
                      >
                        {stationList.Object &&
                          stationList.Object.map((stationItem) => (
                            <option
                              key={stationItem.StationId}
                              value={stationItem.StationId}
                            >
                              {stationItem.StationName}
                            </option>
                          ))}
                      </select>
                    </div> */}
                    <input
                      className="form-control form-control"
                      type="text"
                      placeholder="select location"
                      aria-label=".form-control-sm example"
                    />
                  </form>

                  <div className="door-delivery">
                    <div className="d-flex flex-column">
                      {/* <div className="me-2">
                        <FaTruckPickup className="pickup-truck text-success-1" />
                      </div> */}

                      <h6 className="text-start fs-6">GIGL Lekki Phase 1</h6>

                      <div className="d-flex justify-content-between">
                        <p className="text-muted fs-6 text-start">
                          1 Wole Ariyo St, Lekki Phase 1 105102, Lagos
                        </p>
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            type="radio"
                            name="flexRadioDefault"
                            id="flexRadioDefault1"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>

                <div className="door-delivery mb-4">
                  <div className="d-flex flex-column">
                    {/* <div className="me-2">
                      <FaBuilding className="pickup-truck text-success-1" />
                    </div> */}

                    <h6 className="fs-6 text-start">GIGL Lekki Phase 1</h6>

                    <div className="d-flex justify-content-between">
                      <p className="text-muted fs-6 text-start">
                        1 Wole Ariyo St, Lekki Phase 1 105102, Lagos
                      </p>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault1"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <hr />
                <div className="door-delivery mb-5">
                  <div className="d-flex flex-column">
                    {/* <div className="me-2">
                      <FaBuilding className="pickup-truck text-success-1" />
                    </div> */}

                    <h6 className="fs-6 text-start">GIGL Lekki Phase 1</h6>

                    <div className="d-flex justify-content-between">
                      <p className="text-muted fs-6 text-start">
                        1 Wole Ariyo St, Lekki Phase 1 105102, Lagos
                      </p>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault1"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="d-grid gap-2 btn-success-1 mb-3">
                  <button
                    type="button"
                    className="btn text-capitalize fs-6"
                    onClick={submitHandler}
                  >
                    Choose Pickup Station
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SelectPickupLocation;
