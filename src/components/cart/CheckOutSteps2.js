/* eslint-disable  jsx-a11y/control-has-associated-label */
/* eslint-disable   react/no-unused-prop-types */
/* eslint-disable    react/forbid-prop-types */
/* eslint-disable    no-unused-vars */
import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './CheckOutSteps2.css';

const CheckOutSteps2 = ({ shipping, confirmOrder, payment }) => {
  return (
    <div>
      <div className="progresses py-4">
        <ul className="d-flex align-items-center justify-content-between">
          {shipping ? (
            <li id="step-1" className="blue px-2">
              1
            </li>
          ) : (
            <li id="step-1" className="px-2">
              1
            </li>
          )}

          {confirmOrder ? (
            <li id="step-2" className="blue px-2">
              2
            </li>
          ) : (
            <li id="step-2" className="px-2">
              2
            </li>
          )}

          {payment ? (
            <li id="step-3" className="blue px-2">
              3
            </li>
          ) : (
            <li id="step-3" className="px-2">
              3
            </li>
          )}
        </ul>
        <div className="progress">
          <div
            className="progress-bar"
            role="progressbar"
            style={{ width: '50%' }}
            aria-valuenow="25"
            aria-valuemin="0"
            aria-valuemax="100"
          />
        </div>
      </div>
    </div>
  );
};

CheckOutSteps2.propTypes = {
  shipping: PropTypes.bool.isRequired,
  confirmOrder: PropTypes.bool.isRequired,
  payment: PropTypes.bool.isRequired,
  style: PropTypes.object.isRequired,
};

export default CheckOutSteps2;
