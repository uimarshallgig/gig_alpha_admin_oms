/* eslint-disable  jsx-a11y/click-events-have-key-events */
/* eslint-disable  no-unused-vars */

import React from 'react';
import { Link, useHistory } from 'react-router-dom';

import { MdClear } from 'react-icons/md';

import { FaTruckPickup, FaBuilding } from 'react-icons/fa';
import MetaData from '../shared/MetaData';
import './PickupStation.css';

const DeliveryAddress = () => {
  const history = useHistory();

  const goBack = () => {
    history.goBack();
  };

  const submitHandler = (e) => {
    e.preventDefault();

    history.push('/payment-details');
  };

  const deliveryAddress = localStorage.getItem('deliveryAddress')
    ? JSON.parse(localStorage.getItem('deliveryAddress'))
    : '';
  const localGovt = localStorage.getItem('localGovt')
    ? JSON.parse(localStorage.getItem('localGovt'))
    : '';

  return (
    <>
      <MetaData title="Delivery Address" />

      <section className="main-content pickup-stations">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-sm-6 col-md-4 offset-sm-3 offset-md-4">
              <div className="info-card info-card--danger bg-white text-center mb-4 rounded-lg rounded-2">
                <div className="position-relative">
                  <span className="text-center fw-bold fs-6">Checkout</span>
                  <span className="position-absolute top-0 end-0 close-btn">
                    <MdClear onClick={goBack} />
                  </span>
                </div>
                <hr />
                <div className="d-flex flex-column">
                  <h6 className="d-flex justify-content-start mb-4">
                    Select a delivery method
                  </h6>
                  <div className="door-delivery">
                    <div className="d-flex justify-content-between">
                      <div className="">
                        <FaTruckPickup className="pickup-truck text-success-1 my-auto" />
                      </div>
                      <div className="d-flex flex-column align-self-center text-start">
                        <h6 className="fs-6">{localGovt}</h6>
                        <p className="text-muted fs-6">{deliveryAddress}</p>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault1"
                        />
                      </div>
                    </div>
                  </div>
                  <hr />

                  <div className="door-delivery mb-5">
                    <div className="d-flex justify-content-between">
                      <div className="">
                        <FaBuilding className="pickup-truck text-success-1 my-auto" />
                      </div>
                      <div className="d-flex flex-column align-self-center text-start">
                        <h6 className="fs-6">GIGL Lekki Phase 1</h6>
                        <p className="text-muted fs-6">
                          1 Wole Ariyo St, Lekki Phase 1 105102, Lagos
                        </p>
                      </div>

                      <div className="form-check">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="flexRadioDefault"
                          id="flexRadioDefault1"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="d-grid gap-2 btn-success-1 mb-3">
                  <button
                    type="button"
                    className="btn text-capitalize fs-6"
                    onClick={submitHandler}
                  >
                    Choose Pickup Station
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default DeliveryAddress;
