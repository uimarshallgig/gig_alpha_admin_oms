/* eslint-disable  no-unused-vars */
/* eslint-disable  jsx-a11y/label-has-associated-control */
/* eslint-disable  consistent-return */
/* eslint-disable  no-unneeded-ternary */
/* eslint-disable  no-restricted-syntax */

import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';
import { MdClear, MdKeyboardBackspace } from 'react-icons/md';

// import GMaps from '../maps/GMaps';
import GoogPlace from '../maps/GoogPlace';
import Loader from '../shared/Loader';
import MetaData from '../shared/MetaData';

import './PickupStation.css';
import {
  getStationInfo,
  shipmentPrice,
  getHomeDeliveryAddress,
} from '../../actions/orderActions';

// API key of the google map
const GOOGLE_MAP_API_KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;

// load google map script
const loadGoogleMapScript = (callback) => {
  if (typeof window.google === 'object' && typeof window.google.maps === 'object') {
    callback();
  } else {
    const googleMapScript = document.createElement('script');
    googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAP_API_KEY}&libraries=places`;
    window.document.body.appendChild(googleMapScript);
    googleMapScript.addEventListener('load', callback);
  }
};

const DeliveryDetails = () => {
  const [loadMap, setLoadMap] = useState(false);
  const alert = useAlert();
  // Map redux state to props of Home component
  const { loading, addresses, error } = useSelector((state) => state.homeDelivery);
  const { stationInfo } = useSelector((state) => state.station);
  console.log(stationInfo);
  // console.log(stationInfo.Object);
  console.log(stationInfo && stationInfo.Code);

  const stationList = stationInfo && stationInfo;
  console.log('stationLists', stationList);

  const googlePlacesData = localStorage.getItem('googlePlacesData')
    ? JSON.parse(localStorage.getItem('googlePlacesData'))
    : [];
  console.log('googlePlacesData', googlePlacesData);
  const receiverLocation = googlePlacesData !== null && {
    Latitude: googlePlacesData.lat,
    Longitude: googlePlacesData.lng,
  };

  console.log(receiverLocation);

  // console.log('googlePlacesData address: ', googlePlacesData.address);
  // const [deliveryAdr, setDeliveryAdr] = useState('');
  // console.log(deliveryAdr);
  const [receiverName, setReceiverName] = useState('');
  const [receiverEmail, setReceiverEmail] = useState('');

  const [receiverPhoneNumber, setReceiverPhoneNumber] = useState('');
  const [place, setPlace] = useState(null);
  const [receiverAddress, setReceiverAddress] = useState('');
  // const [receiverLocation, setReceiverLocation] = useState({
  //   latitude: googlePlacesData.lat,
  //   longitude: googlePlacesData.lng,
  // });
  const [station, setStation] = useState(stationInfo.StateName);
  localStorage.setItem('receiverStationId', JSON.stringify(station));

  const dispatch = useDispatch();
  const history = useHistory();

  // const { loading: shipmentLoader, shipmentPriceDetails } = useSelector(
  //   (state) => state.shippingPrice
  // );
  // console.log(shipmentPriceDetails);

  // const shipmentPriceData = shipmentPriceDetails && shipmentPriceDetails?.Object;

  // localStorage.setItem('shipmentPriceData', JSON.stringify(shipmentPriceDetails));

  // console.log('shipment data for deliveryFee from deliverDetails: ', shipmentPriceData);

  useEffect(() => {
    loadGoogleMapScript(() => {
      setLoadMap(true);
    });
    if (error) {
      return alert.error(error);
    }

    dispatch(getHomeDeliveryAddress());
    dispatch(getStationInfo());
  }, [dispatch, alert, error]);

  // const { ReceiverName, ReceiverAddress, ReceiverPhoneNumber, ReceiverLocation } =
  //   shipmentInfo;

  const stationItems = localStorage.getItem('stationItems')
    ? JSON.parse(localStorage.getItem('stationItems'))
    : [];
  const productPrice = localStorage.getItem('productPrice')
    ? JSON.parse(localStorage.getItem('productPrice'))
    : '';
  const orderQty = localStorage.getItem('orderQty')
    ? JSON.parse(localStorage.getItem('orderQty'))
    : [];
  console.log('Price and Qty from DeliveryDetails: ', productPrice, orderQty);

  const productTotalPrice = stationItems.price * orderQty;
  console.log('total price', productTotalPrice);

  console.log(stationItems);
  console.log(stationItems.productId);
  console.log(stationItems.productName);
  console.log(stationItems.address);
  console.log(stationItems.lat);
  console.log(stationItems.long);
  console.log(stationItems.locality);
  console.log(stationItems.neighborhood);
  console.log(stationItems.merchantPhoneNumber);
  console.log(stationItems.merchantStationId);
  console.log(stationItems.name);
  console.log(stationItems.customerCode);
  console.log(stationItems.quantity);
  console.log(stationItems.price);
  console.log(stationItems.userId);
  console.log(stationItems.weight);

  const senderLocation = {
    Latitude: stationItems.lat,
    Longitude: stationItems.long,
  };
  const receiverLoc = {
    Latitude: '6.5483775',
    Longitude: '3.3883414',
  };

  const preShipmentItems = [
    {
      SpecialPackageId: '0',
      Quantity: String(orderQty),
      Weight: String(stationItems.weight),
      ItemType: 'Normal',
      WeightRange: '0',
      ItemName: stationItems.productName,
      Value: String(productTotalPrice),
      ShipmentType: 'Regular',
    },
  ];

  const merchantDetails = {
    CustomerCode: stationItems.customerCode,
    // CustomerCode: 'IND265936',
    SenderName: stationItems.name,
    SenderPhoneNumber: stationItems.merchantPhoneNumber,
    SenderStationId: Number(stationItems.merchantStationId),
    SenderLocality: stationItems.locality,
    SenderAddress: stationItems.address,
    SenderLocation: senderLocation,
    PaymentType: 'Cash',
    PreShipmentItems: preShipmentItems[0],
  };

  console.log('MerchantDetails', merchantDetails);

  const handleAddressChange = (e) => {
    const item = {};
    // Check if item is already in array by using productId
    // const isItemExist = item.find((i) => i.product === item.product);
    // setDeliveryAdr({
    //   [e.target.name]: e.target.value,
    // });
    // setReceiverAddress(e.target.value);
  };

  const submitHandler = (e) => {
    e.preventDefault();

    const apiData = {
      ReceiverName: receiverName,
      // ReceiverPhoneNumber: '08039322440',
      ReceiverPhoneNumber: receiverPhoneNumber,
      // ReceiverEmail: 'string',
      ReceiverEmail: receiverEmail,
      ReceiverAddress:
        'Dominos Pizza Gbagada,1A Idowu Olaitan St, Gbagada, Lagos, Nigeria',
      // ReceiverStationId: 4,
      ReceiverStationId: Number(station),
      ReceiverLocation: {
        Latitude: '6.5483775',
        Longitude: '3.3883414',
      },
      Merchants: [
        {
          // CustomerCode: 'ECO001449',
          CustomerCode: stationItems.customerCode,
          // SenderName: 'TEST ECOMMERCE IT',
          SenderName: stationItems.name,
          // SenderPhoneNumber: '+2347063965528',
          SenderPhoneNumber: stationItems.merchantPhoneNumber,
          // SenderStationId: 4,
          SenderStationId: Number(stationItems.merchantStationId),
          // SenderLocality: 'Ifako Ijaye',
          SenderLocality: stationItems.neighborhood,
          // SenderAddress: '21 Emmanuel Olorunfemi St, Ifako Agege, Lagos, Nigeria',
          SenderAddress: stationItems.address,
          // SenderLocation: {
          //   Latitude: '6.639438',
          //   Longitude: '3.330983',
          // },
          SenderLocation: senderLocation,
          PaymentType: 'Cash',
          PreShipmentItems: [
            {
              SpecialPackageId: '0',
              Quantity: String(orderQty), // qty purchased
              // Weight: '1',
              Weight: stationItems.weight,
              ItemType: 'Normal',
              WeightRange: '0',
              // ItemName: 'Shoe Lace 2',
              ItemName: stationItems.productName,
              // Value: '1000',
              Value: String(stationItems.price),
              ShipmentType: 'Regular',
            },
          ],
        },
      ],
      PickupOptions: 'HOMEDELIVERY',
      IsHomeDelivery: true,
      VehicleType: 'BIKE',
      PaymentType: 'Cash',
    };

    localStorage.setItem('shipmentData', JSON.stringify(apiData));

    const formData = new FormData();
    formData.set('ReceiverName', receiverName);
    formData.set('ReceiverPhoneNumber', receiverPhoneNumber);
    formData.set('ReceiverAddress', 'Adebakin close');
    // formData.set('ReceiverAddress', receiverAddress);
    formData.set('ReceiverEmail', receiverEmail);
    formData.set('ReceiverStationId', Number(station));
    // formData.set('ReceiverLocation', receiverLocation);
    formData.set('ReceiverLocation', receiverLoc);
    formData.set('Merchants', merchantDetails[0]);

    // TODO: pick from radio dynamically
    formData.set('PickupOptions', 'HOMEDELIVERY');
    // TODO: pick from state based on radio choice
    formData.set('IsHomeDelivery', true);

    formData.set('VehicleType', 'BIKE');
    formData.set('PaymentType', 'Cash');

    // dispatch(shipmentPrice(formData));
    dispatch(shipmentPrice(apiData));
    for (const pair of formData.entries()) {
      console.log(`${pair[0]} - ${pair[1]}`);
    }

    history.push('/delivery-address');
  };

  const goBack = () => {
    history.goBack();
  };

  const deliveryAddr = addresses && addresses;
  const listOfDeliveryAddresses = [];

  addresses?.map((addr) => {
    return listOfDeliveryAddresses.push(addr.LGAName);
  });
  console.log('Home Delivery addr', deliveryAddr);
  console.log(listOfDeliveryAddresses);
  console.log(googlePlacesData);
  const googleApiAddress = googlePlacesData.localGovt;
  // googlePlacesData will now change to the place obj in the state
  const googleApiAddress1 = place ? place.localGovt : [];

  console.log(googleApiAddress);

  console.log(googleApiAddress1);

  const addressMatch = googleApiAddress1.filter((addr) => {
    return listOfDeliveryAddresses?.find((selected) => selected === addr.long_name);
  });
  console.log(addressMatch);
  console.log(addressMatch.length);
  console.log(addressMatch[0]?.long_name);

  const isAddressExist = place !== null ? addressMatch.length : '';
  console.log(isAddressExist);
  if (isAddressExist === 0) {
    alert.info('The address is not within GIG Home delivery,choose from Pickup stations');

    // history.push('/pickup-stations');
  }

  const placeInputRef = useRef(null);

  const initPlaceAPI = () => {
    const autocomplete = new window.google.maps.places.Autocomplete(
      placeInputRef.current
    );

    window.google.maps.event.addListener(autocomplete, 'place_changed', function () {
      const places = autocomplete.getPlace();
      setPlace({
        address: places.formatted_address,
        lat: places.geometry.location.lat(),
        lng: places.geometry.location.lng(),
        localGovt: places.address_components,
      });
    });
  };
  console.log(place);
  const inputFieldValue = place && place?.address;
  console.log(inputFieldValue);
  const deliveryAddressStorage = place && place.address;
  const localGovt = place && place.localGovt[1].long_name;
  console.log(localGovt);

  localStorage.setItem('deliveryAddress', JSON.stringify(deliveryAddressStorage));
  localStorage.setItem('localGovt', JSON.stringify(localGovt));

  return (
    <>
      <MetaData title="Selected Pickup Location" />

      {loading && stationList.length === 0 ? (
        <Loader />
      ) : (
        <section className="main-content pickup-stations">
          <p>{receiverName}</p>
          <p>{receiverEmail}</p>
          {/* <p>{deliveryAdr}</p> */}
          <p>{receiverPhoneNumber}</p>
          <p>{station}</p>
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-sm-6 col-md-4 offset-sm-3 offset-md-4">
                <div className="info-card info-card--danger bg-white mb-4 rounded-lg rounded-2">
                  <div className="position-relative">
                    <span className="text-center d-block fw-bold fs-6 text-uppercase">
                      enter delivery details
                    </span>
                    <span className="position-absolute top-0 end-0 close-btn">
                      <MdClear onClick={goBack} />
                    </span>
                    <span className="position-absolute top-0 start-0 close-btn">
                      <MdKeyboardBackspace onClick={goBack} />
                    </span>
                  </div>
                  <hr />
                  <div className="d-flex flex-column">
                    {/* <form className="mb-4">
                      <input
                        className="form-control form-control"
                        type="text"
                        placeholder="select location"
                        aria-label=".form-control-sm example"
                      />
                      
                    </form> */}

                    <form onSubmit={submitHandler}>
                      <div className="mb-3">
                        <label htmlFor="name" className="form-label">
                          Full Name
                        </label>
                        <input
                          name="name"
                          type="text"
                          className="form-control"
                          id="name"
                          placeholder="Full Name"
                          value={receiverName}
                          onChange={(e) => setReceiverName(e.target.value)}
                          required
                        />
                      </div>
                      <div className="mb-3">
                        <label htmlFor="email" className="form-label">
                          Email
                        </label>
                        <input
                          name="email"
                          type="email"
                          className="form-control"
                          id="email"
                          placeholder="name@example.com"
                          value={receiverEmail}
                          onChange={(e) => setReceiverEmail(e.target.value)}
                          required
                        />
                      </div>
                      <div className="mb-3">
                        <label htmlFor="deliveryAddress" className="form-label">
                          Delivery address
                        </label>

                        {/* <GMaps
                          type="text"
                          placeholder="Delivery address"
                          name="delivery_address"
                          value={receiverAddress}
                          onChange={handleAddressChange}
                          required
                        /> */}
                        {!loadMap ? (
                          <Loader />
                        ) : (
                          <GoogPlace
                            type="text"
                            placeholder="Delivery address"
                            name="deliveryAddress"
                            value={place && place?.address}
                            onChange={handleAddressChange}
                            initPlaceAPI={initPlaceAPI}
                            placeInputRef={placeInputRef}
                            required
                          />
                        )}
                      </div>
                      <div className="mb-3">
                        <label htmlFor="phone_field" className="form-label">
                          Phone Number
                        </label>
                        <input
                          type="phone"
                          id="phone_field"
                          className="form-control"
                          placeholder="+234************"
                          value={receiverPhoneNumber}
                          onChange={(e) => setReceiverPhoneNumber(e.target.value)}
                          required
                        />
                      </div>

                      <div className="form-group mb-3">
                        <label htmlFor="station_field">Stations</label>
                        <select
                          id="station_field"
                          className="form-control"
                          value={station}
                          onChange={(e) => setStation(e.target.value)}
                          required
                        >
                          {stationList.Object &&
                            stationList.Object.map((stationItem) => (
                              <option
                                key={stationItem.StationId}
                                value={stationItem.StationId}
                              >
                                {stationItem.StationName}
                              </option>
                            ))}
                        </select>
                      </div>

                      {/* <div className="mb-3">
                        <label htmlFor="delivery_address" className="form-label">
                          Delivery address
                        </label>
                        <textarea
                          className="form-control"
                          placeholder="Delivery address"
                          id="delivery_address"
                          rows="3"
                        />
                      </div> */}

                      {place && (
                        <div style={{ marginTop: 20, lineHeight: '25px' }}>
                          <div style={{ marginBottom: 10 }}>
                            <b>Selected Place</b>
                          </div>
                          <div>
                            <b>Address:</b> {place.address}
                          </div>
                          <div>
                            <b>Lat:</b> {place.lat}
                          </div>
                          <div>
                            <b>Lng:</b> {place.lng}
                          </div>
                          <div>
                            <b>LocalGovt:</b>{' '}
                            {/* {place.localGovt.map((item) => {
                              return (
                                <div key={item.short_name}>
                                  <p>{item.long_name}</p>
                                </div>
                              );
                            })} */}
                          </div>
                        </div>
                      )}
                      {/* {place &&
                        localStorage.setItem('googlePlacesData', JSON.stringify(place))} */}
                      <div className="d-grid gap-2 btn-success-1 mb-3">
                        <button
                          type="submit"
                          className="btn text-capitalize fs-6"
                          disabled={loading ? true : false}
                        >
                          {loading && (
                            <i className="fas fa-spinner fa-pulse text-muted" />
                          )}
                          Choose Pickup Station
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
};

export default DeliveryDetails;
