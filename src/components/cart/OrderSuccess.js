import React from 'react';
// import { Link } from 'react-router-dom';
import MetaData from '../shared/MetaData';
import './OrderSuccess.css';

const OrderSuccess = () => {
  return (
    <>
      <MetaData title="Order Success" />

      <section className="main-content">
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-md-4 offset-sm-3 offset-md-4">
              <div className="info-card info-card--danger bg-white text-center mb-4 rounded-lg rounded-4">
                <div className="info-card_icon mx-auto mb-4 d-flex justify-content-center align-items-center position-relative">
                  {/* <i className="fa fa-trash" /> */}
                  <img
                    className="my-5 img-fluid d-block mx-auto p-2"
                    src="/assets/images/payment_successful.svg"
                    alt="Order Success"
                    width="200"
                    height="200"
                  />
                </div>
                <h2 className="mb-4 text-success">Payment Successful</h2>
                <hr />
                <div className="mb-4 text-muted">
                  <p>Download GIG GO APP</p>
                </div>
                <div className="d-flex">
                  <button type="button" className="btn btn-dark w-50 me-1">
                    <img
                      className="img-fluid d-block"
                      src="/assets/images/app_store.png"
                      alt="Order Success"
                    />
                  </button>
                  <button type="button" className="btn btn-dark w-50 ms-1">
                    <img
                      className="img-fluid d-block"
                      src="/assets/images/google_play.png"
                      alt="Order Success"
                    />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* <div className="row justify-content-center">
        <div className="col-6 mt-5 text-center">
          <img
            className="my-5 img-fluid d-block mx-auto"
            src="/assets/images/order_success.png"
            alt="Order Success"
            width="200"
            height="200"
          />

          <h2>Your Order has been placed successfully.</h2>

          <Link to="/orders/me">Go to Orders</Link>
        </div>
      </div> */}
    </>
  );
};

export default OrderSuccess;
