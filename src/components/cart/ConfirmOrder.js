/* eslint-disable  jsx-a11y/label-has-associated-control */
/* eslint-disable  react/self-closing-comp */
/* eslint-disable  no-unused-vars */
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import MyGoogleMaps from '../maps/MyGoogleMaps';

import MetaData from '../shared/MetaData';
import Modal from '../shared/Modal';
import CheckOutSteps from './CheckOutSteps';
import CheckOutSteps2 from './CheckOutSteps2';
import Payment from './Payment';
import GMaps from '../maps/GMaps';

const ConfirmOrder = () => {
  const [show, showModal] = useState(false);
  const history = useHistory();

  const { isAuthenticated, error, loading } = useSelector((state) => state.shipping);

  const orderAmount = Number(localStorage.getItem('shippingCalc')).toFixed(2);
  const deliveryFee = Number(500).toFixed(2);
  let totalOrderAmount = Number(orderAmount) + Number(deliveryFee);
  totalOrderAmount = totalOrderAmount.toFixed(2);
  const dispatch = useDispatch();
  // dispatch(orderShipmentLogin());
  if (isAuthenticated) {
    console.log('I am in');
  }

  const submitHandler = (e) => {
    e.preventDefault();

    history.push('/payment');
  };

  return (
    <>
      <MetaData title="Confirm Order" />
      {/* <MyGoogleMaps /> */}
      <GMaps />
      {/* <CheckOutSteps shipping confirmOrder /> */}

      <div className="row wrapper mt-5" id="shipping">
        <div className="col-10 col-lg-5">
          <div className="card bg-white shadow-lg mb-4">
            <h5 className="text-center pt-2 h3">Checkout</h5>
            <hr className="w-75 mx-auto text-muted border border-2" />
            <CheckOutSteps2 shipping confirmOrder />

            <div className="card-body px-1">
              <form className="" onSubmit={submitHandler}>
                <div className="mb-3">
                  <label htmlFor="name" className="form-label">
                    Full Name
                  </label>
                  <input
                    name="name"
                    type="text"
                    className="form-control"
                    id="name"
                    placeholder="Full Name"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="email" className="form-label">
                    Email address
                  </label>
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    id="email"
                    placeholder="name@example.com"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="phone_field" className="form-label">
                    Phone Number
                  </label>
                  <input
                    type="phone"
                    id="phone_field"
                    className="form-control"
                    placeholder="+234************"
                    required
                  />
                </div>

                <div className="mb-3">
                  <label htmlFor="delivery_address" className="form-label">
                    Delivery address
                  </label>
                  <textarea
                    className="form-control"
                    placeholder="Delivery address"
                    id="delivery_address"
                    rows="3"
                  ></textarea>
                </div>

                {/* Price/quantity */}
                <div className="d-flex justify-content-between price-quantity">
                  <p>Amount</p>
                  <div className="stockCounter d-inline">&#8358;{orderAmount}</div>
                </div>
                <div className="d-flex justify-content-between price-quantity">
                  <p>Delivery fee</p>
                  <div className="stockCounter d-inline">&#8358;{deliveryFee}</div>
                </div>
                <hr />
                <div className="d-flex justify-content-between price-quantity">
                  <p>Total amount</p>
                  <div className="stockCounter d-inline">&#8358;{totalOrderAmount}</div>
                </div>
                {/* Checkout btn */}
                <div className="d-grid gap-2">
                  <button
                    type="button"
                    className="btn btn-dark d-inline ms-4 rounded-pill text-uppercase fw-bold"
                    onClick={submitHandler}
                    // onClick={() => showModal(true)}
                  >
                    proceed
                  </button>
                </div>
                {/* <Modal show={show} handleClose={() => showModal(false)}>
                  <Payment />
                </Modal> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ConfirmOrder;
