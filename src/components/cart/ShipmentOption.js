import React from 'react';
import { useHistory } from 'react-router-dom';
import './OrderSuccess.css';

const ShipmentOption = () => {
  const history = useHistory();

  const submitHandler = (e) => {
    e.preventDefault();

    history.push('/shipping');
  };
  return (
    <>
      <section className="main-content mt-5">
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-md-4 offset-sm-3 offset-md-4">
              <div className="info-card info-card--danger bg-white text-center mb-4 rounded-lg rounded-4">
                <div className="mx-auto mb-4 d-flex justify-content-center align-items-center position-relative">
                  <h1 className="h5">Choose Preferred Option</h1>
                </div>

                <hr />

                <div className="d-flex">
                  <button
                    type="button"
                    className="btn btn-dark w-50 me-1"
                    onClick={submitHandler}
                  >
                    Doorstep
                  </button>
                  <button type="button" className="btn btn-dark w-50 ms-1">
                    Service centres
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ShipmentOption;
