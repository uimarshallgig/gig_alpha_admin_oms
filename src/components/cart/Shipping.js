/* eslint-disable  jsx-a11y/label-has-associated-control */
/* eslint-disable  no-unused-vars */

import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import MetaData from '../shared/MetaData';
import Modal from '../shared/Modal';
import CheckOutSteps from './CheckOutSteps';

import ConfirmOrder from './ConfirmOrder';
import { saveShippingInfo } from '../../actions/cartActions';
import CheckOutSteps2 from './CheckOutSteps2';

const Shipping = () => {
  // shippingInfo is stored in the state in store.js
  const { shippingInfo } = useSelector((state) => state.cart);

  const dispatch = useDispatch();

  const [fullName, setFullName] = useState(shippingInfo.full_name);
  const [email, setEmail] = useState(shippingInfo.full_name);
  const [address, setAddress] = useState(shippingInfo.address);
  const [phoneNo, setPhoneNo] = useState(shippingInfo.phoneNo);

  const [show, showModal] = useState(false);
  const history = useHistory();

  // Calculate Order Prices
  const orderAmount = Number(localStorage.getItem('shippingCalc')).toFixed(2);
  const orderQuantity = Number(localStorage.getItem('orderQty'));
  // const itemsPrice = qty * productPrice;
  // const shippingPrice = itemsPrice > 200 ? 0 : 25;

  // const totalPrice = (itemsPrice + shippingPrice).toFixed(2);
  // localStorage.setItem('shippingCalc', totalPrice);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(saveShippingInfo({ fullName, email, address, phoneNo }));

    history.push('/order/confirm');
  };

  return (
    <>
      <MetaData title="Shipping Info" />

      <div className="row wrapper mt-5" id="shipping">
        <div className="col-10 col-lg-5">
          <div className="card bg-white shadow-lg mb-4">
            <h5 className="text-center pt-2 h3">Checkout</h5>
            <CheckOutSteps2 shipping />
            <hr className="w-75 mx-auto text-muted border border-2" />

            <div className="card-body px-1">
              <div className="card-text d-flex flex-column size">
                <h5 className="card-title">Size</h5>
                <div className="mb-2">
                  <span className="badge fs-6 bg-light text-dark me-3 px-2 py-2">S</span>
                  <span className="badge fs-6 bg-light text-dark me-3 px-2 py-2">M</span>
                  <span className="badge fs-6 bg-light text-dark me-3 px-2 py-2">L</span>
                  <span className="badge fs-6 bg-light text-dark me-3 px-2 py-2">XL</span>
                </div>
              </div>
              {/* colors */}
              <div className="d-flex justify-content-start color">
                <p className="me-2">Color</p>
                <div className="color-choice d-flex justify-content-around">
                  <div className="color-box color-1" />
                  <div className="color-box color-2" />
                  <div className="color-box color-3" />
                  <div className="color-box color-4" />
                </div>
              </div>
              <form className="" onSubmit={submitHandler}>
                <div className="row mb-3">
                  <label htmlFor="quantity" className="col-sm-2 col-form-label">
                    Qty
                  </label>
                  <div className="col-sm-4">
                    <input
                      type="number"
                      className="form-control
                     count"
                      id="quantity"
                      value={orderQuantity}
                    />
                  </div>
                </div>

                {/* Price/quantity */}
                <div className="d-flex justify-content-between price-quantity">
                  <p>Amount</p>
                  <div className="stockCounter d-inline">&#8358;{orderAmount}</div>
                </div>
                {/* Checkout btn */}
                <div className="d-grid gap-2">
                  <button
                    type="button"
                    className="btn btn-dark d-inline ms-4 rounded-pill text-uppercase fw-bold"
                    onClick={submitHandler}
                    // onClick={() => showModal(true)}
                  >
                    proceed
                  </button>
                </div>
                <Modal show={show} handleClose={() => showModal(false)}>
                  <ConfirmOrder />
                </Modal>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Shipping.propTypes = {
  // qty: PropTypes.number.isRequired,
  // productPrice: PropTypes.number.isRequired,
};

export default Shipping;
