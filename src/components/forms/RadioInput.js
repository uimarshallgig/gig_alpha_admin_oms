import PropTypes from 'prop-types';

const RadioInput = ({ label, value, checked, setter }) => {
  return (
    <label htmlFor={label}>
      <input type="radio" checked={checked === value} onChange={() => setter(value)} />
      <span>{label}</span>
    </label>
  );
};

RadioInput.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  setter: PropTypes.func.isRequired,
};
